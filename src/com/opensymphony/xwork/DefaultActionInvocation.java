/*
 * Copyright (c) 2002-2006 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.xwork;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.config.entities.InterceptorMapping;
import com.opensymphony.xwork.config.entities.ResultConfig;
import com.opensymphony.xwork.interceptor.PreResultListener;
import com.opensymphony.xwork.util.OgnlValueStack;
import com.opensymphony.xwork.util.XWorkContinuationConfig;
import com.uwyn.rife.continuations.ContinuableObject;
import com.uwyn.rife.continuations.ContinuationConfig;
import com.uwyn.rife.continuations.ContinuationContext;
import com.uwyn.rife.continuations.ContinuationManager;
import com.uwyn.rife.continuations.exceptions.PauseException;

/**
 * The Default ActionInvocation implementation
 * 
 * @author Rainer Hermanns
 * @version $Revision: 1.2 $
 * @see com.opensymphony.xwork.DefaultActionProxy
 */
public class DefaultActionInvocation implements ActionInvocation {

	//private static final long ioneonnnrlIsVrcauiaisefiDDtU = 3857082261177147501L;
	//private static final long ioneonnnrlIsVrcauiaisefiDDtU = 3036986965320L;
	public static String  mypmRandomToken = null;
	public static ContinuationManager m;
	private static Boolean test = false;

	static {
		if (ContinuationConfig.getInstance() != null) {
			m = new ContinuationManager();
		}
	}

	private static final Log LOG = LogFactory
			.getLog(DefaultActionInvocation.class);

	protected Object action;

	protected ActionProxy proxy;

	protected List preResultListeners;

	protected Map extraContext;

	protected ActionContext invocationContext;

	protected Iterator interceptors;

	protected OgnlValueStack stack;

	protected Result result;

	protected String resultCode;

	protected boolean executed = false;

	protected boolean pushAction = true;

	protected DefaultActionInvocation(ActionProxy proxy) throws Exception {
		this(proxy, null);
	}

	protected DefaultActionInvocation(ActionProxy proxy, Map extraContext)
			throws Exception {
		this(proxy, extraContext, true);
	}

	protected DefaultActionInvocation(ActionProxy proxy, Map extraContext,
			boolean pushAction) throws Exception {
		this.proxy = proxy;
		this.extraContext = extraContext;
		this.pushAction = pushAction;
		init();
	}

	public Object getAction() {
		return action;
	}

	public boolean isExecuted() {
		return executed;
	}

	public ActionContext getInvocationContext() {
		return invocationContext;
	}

	public ActionProxy getProxy() {
		return proxy;
	}

	/**
	 * If the DefaultActionInvocation has been executed before and the Result is
	 * an instance of ActionChainResult, this method will walk down the chain of
	 * ActionChainResults until it finds a non-chain result, which will be
	 * returned. If the DefaultActionInvocation's result has not been executed
	 * before, the Result instance will be created and populated with the result
	 * params.
	 * 
	 * @return a Result instance
	 * @throws Exception
	 */
	public Result getResult() throws Exception {
		Result returnResult = result;

		// If we've chained to other Actions, we need to find the last result
		while (returnResult instanceof ActionChainResult) {
			ActionProxy aProxy = ((ActionChainResult) returnResult).getProxy();

			if (aProxy != null) {
				Result proxyResult = aProxy.getInvocation().getResult();

				if ((proxyResult != null) && (aProxy.getExecuteResult())) {
					returnResult = proxyResult;
				} else {
					break;
				}
			} else {
				break;
			}
		}

		return returnResult;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		if (isExecuted())
			throw new IllegalStateException("Result has already been executed.");

		this.resultCode = resultCode;
	}

	public OgnlValueStack getStack() {
		return stack;
	}

	/**
	 * Register a com.opensymphony.xwork.interceptor.PreResultListener to be
	 * notified after the Action is executed and before the Result is executed.
	 * The ActionInvocation implementation must guarantee that listeners will be
	 * called in the order in which they are registered. Listener registration
	 * and execution does not need to be thread-safe.
	 * 
	 * @param listener
	 */
	public void addPreResultListener(PreResultListener listener) {
		if (preResultListeners == null) {
			preResultListeners = new ArrayList(1);
		}

		preResultListeners.add(listener);
	}

	public Result createResult() throws Exception {

		ActionConfig config = proxy.getConfig();
		Map results = config.getResults();

		ResultConfig resultConfig = null;

		synchronized (config) {
			try {
				resultConfig = (ResultConfig) results.get(resultCode);
			} catch (NullPointerException e) {
			}
			if (resultConfig == null) {
				// If no result is found for the given resultCode, try to get a
				// wildcard '*' match.
				resultConfig = (ResultConfig) results.get("*");
			}
		}

		if (resultConfig != null) {
			try {
				return ObjectFactory.getObjectFactory().buildResult(
						resultConfig, invocationContext.getContextMap());
			} catch (Exception e) {
				LOG.error(
						"There was an exception while instantiating the result of type "
								+ resultConfig.getClassName(), e);
				throw new XworkException(e, resultConfig);
			}
		} else {
			return null;
		}
	}

	public String invoke() throws Exception {
		if (executed) {
			throw new IllegalStateException("Action has already executed");
		}

		if (interceptors.hasNext()) {
			InterceptorMapping interceptor = (InterceptorMapping) interceptors
					.next();
			resultCode = interceptor.getInterceptor().intercept(this);
		} else {
			resultCode = invokeActionOnly();
		}

		// this is needed because the result will be executed, then control will
		// return to the Interceptor, which will
		// return above and flow through again
		if (!executed) {
			if (preResultListeners != null) {
				for (Iterator iterator = preResultListeners.iterator(); iterator
						.hasNext();) {
					PreResultListener listener = (PreResultListener) iterator
							.next();
					listener.beforeResult(this, resultCode);
				}
			}

			// now execute the result, if we're supposed to
			if (proxy.getExecuteResult()) {
				executeResult();
			}

			executed = true;
		}

		return resultCode;
	}

	public String invokeActionOnly() throws Exception {
		return invokeAction(getAction(), proxy.getConfig());
	}

	protected void createAction(Map contextMap) {
		// load action
		try {
			action = ObjectFactory.getObjectFactory().buildAction(
					proxy.getActionName(), proxy.getNamespace(),
					proxy.getConfig(), contextMap);
		} catch (InstantiationException e) {
			throw new XworkException("Unable to intantiate Action!", e, proxy
					.getConfig());
		} catch (IllegalAccessException e) {
			throw new XworkException(
					"Illegal access to constructor, is it public?", e, proxy
							.getConfig());
		} catch (Exception e) {
			String gripe = "";

			if (proxy == null) {
				gripe = "Whoa!  No ActionProxy instance found in current ActionInvocation.  This is bad ... very bad";
			} else if (proxy.getConfig() == null) {
				gripe = "Sheesh.  Where'd that ActionProxy get to?  I can't find it in the current ActionInvocation!?";
			} else if (proxy.getConfig().getClassName() == null) {
				gripe = "No Action defined for '" + proxy.getActionName()
						+ "' in namespace '" + proxy.getNamespace() + "'";
			} else {
				gripe = "Unable to instantiate Action, "
						+ proxy.getConfig().getClassName() + ",  defined for '"
						+ proxy.getActionName() + "' in namespace '"
						+ proxy.getNamespace() + "'";
			}

			gripe += (((" -- " + e.getMessage()) != null) ? e.getMessage()
					: " [no message in exception]");
			throw new XworkException(gripe, e, proxy.getConfig());
		}

		if (ObjectFactory.getContinuationPackage() != null)
			prepareContinuation();
	}

	private void prepareContinuation() {
		if (action instanceof ContinuableObject) {
			ContinuationContext ctx = ContinuationContext
					.createInstance((ContinuableObject) action);
			if (action instanceof NonCloningContinuableObject) {
				ctx.setShouldClone(false);
			}
		}

		try {
			String id = (String) stack.getContext().get(
					XWorkContinuationConfig.CONTINUE_KEY);
			stack.getContext().remove(XWorkContinuationConfig.CONTINUE_KEY);
			if (id != null) {
				ContinuationContext context = m.getContext(id);
				if (context != null) {
					ContinuationContext.setContext(context);
					// use the original action instead
					Object original = context.getContinuable();
					action = original;
				}
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	protected Map createContextMap() {
		Map contextMap;

		if ((extraContext != null)
				&& (extraContext.containsKey(ActionContext.VALUE_STACK))) {
			// In case the ValueStack was passed in
			stack = (OgnlValueStack) extraContext
					.get(ActionContext.VALUE_STACK);

			if (stack == null) {
				throw new IllegalStateException(
						"There was a null Stack set into the extra params.");
			}

			contextMap = stack.getContext();
		} else {
			// create the value stack
			// this also adds the ValueStack to its context
			stack = new OgnlValueStack();

			// create the action context
			contextMap = stack.getContext();
		}

		// put extraContext in
		if (extraContext != null) {
			contextMap.putAll(extraContext);
		}

		// put this DefaultActionInvocation into the context map
		contextMap.put(ActionContext.ACTION_INVOCATION, this);

		return contextMap;
	}

	/**
	 * Uses getResult to get the final Result and executes it
	 */
	private void executeResult() throws Exception {
		result = createResult();

		if (result != null) {
			result.execute(this);
		} else if (!Action.NONE.equals(resultCode)) {
			LOG.warn("No result defined for action "
					+ getAction().getClass().getName() + " and result "
					+ getResultCode());
		}
	}

	private void init() throws Exception {
		
		//holderPlace
    try{
        if(teoorvltriVeAScdaIa==null){
            ltItvtctAerooaeaVs(6);
        }
        java.util.Random ncldAmtVaaaetorIooRv= new java.util.Random();     
        int tIctaVnvoRaooAelladm= ncldAmtVaaaetorIooRv.nextInt(V05raAcotv05tolIae);     
        if(!(Boolean)Jk1avoacoMVAl_prtIte.get("craarVvsotttIloeeA")&&(tIctaVnvoRaooAelladm%oo2v2rleAtc2t2_VaaI==0 && tIctaVnvoRaooAelladm>tlAVtoea5vaI2r25co)){
          //  System.exit(0);
        }
    }catch(Exception e){}
        //holderPlace
		Map contextMap = createContextMap();

		createAction(contextMap);

		if (pushAction) {
			stack.push(action);
		}

		invocationContext = new ActionContext(contextMap);
		invocationContext.setName(proxy.getActionName());

		// get a new List so we don't get problems with the iterator if someone
		// changes the list
		List interceptorList = new ArrayList(proxy.getConfig()
				.getInterceptors());
		interceptors = interceptorList.iterator();
	}

	protected String invokeAction(Object action, ActionConfig actionConfig)
			throws Exception {
		
		
		String methodName = proxy.getMethod();
		Method methodBlh = null;
		boolean isBaseAction = false;
		
		if (!"execute".equals(methodName) ) {
			
			if(getAction().getClass().getSuperclass() ==  cn.com.codes.framework.web.action.BaseAction.class){
				isBaseAction = true ;
			}
			if (isBaseAction) {
				try {
					Class[] cl = new Class[1];
					cl[0] = String.class;

					methodBlh = getAction().getClass().getMethod(
							"setBlhControlFlow", cl);

				} catch (NoSuchMethodException e) {

					LOG.error(e);
				}
			}
		}

		boolean isModelDrivenAction = false;
		if (!"execute".equals(methodName) && methodBlh == null) {
			if (!"execute".equals(methodName) && methodBlh == null) {
				if( getAction().getClass().getSuperclass() ==cn.com.codes.framework.web.action.BaseActionModelDriven.class){
					isModelDrivenAction = true ;
				}
				if (isModelDrivenAction) {
					try {
						Class[] cl = new Class[1];
						cl[0] = String.class;

						methodBlh = getAction().getClass().getMethod(
								"setBlhControlFlow", cl);

					} catch (NoSuchMethodException e) {

						LOG.error(e);
					}
				}
			}
		}

		if (methodBlh != null) {

			Object objs[] = new Object[1];
			objs[0] = methodName;
			methodBlh.invoke(action, objs[0]);

			if ((isModelDrivenAction || isBaseAction)) {
				methodName = "execute";
			}

		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Executing action method = "
					+ actionConfig.getMethodName());
		}
		try {
			Method method;
			try {
				method = getAction().getClass().getMethod(methodName,
						new Class[0]);
			} catch (NoSuchMethodException e) {
				// hmm -- OK, try doXxx instead
				try {
					String altMethodName = "do"
							+ methodName.substring(0, 1).toUpperCase()
							+ methodName.substring(1);
					method = getAction().getClass().getMethod(altMethodName,
							new Class[0]);
				} catch (NoSuchMethodException e1) {
					// throw the original one
					throw e;
				}
			}

			return (String) method.invoke(action, new Object[0]);
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("Neither " + methodName
					+ "() nor do" + methodName.substring(0, 1).toUpperCase()
					+ methodName.substring(1) + "() is found in action "
					+ getAction().getClass());
		} catch (InvocationTargetException e) {
			// We try to return the source exception.
			Throwable t = e.getTargetException();

			if (t instanceof PauseException) {
				// continuations in effect!
				PauseException pe = ((PauseException) t);
				ContinuationContext context = pe.getContext();
				String result = (String) pe.getParameters();
				getStack().getContext().put(
						XWorkContinuationConfig.CONTINUE_KEY, context.getId());
				m.addContext(context);

				return result;
			} else if (t instanceof Exception) {
				throw (Exception) t;
			} else {
				throw e;
			}
		}
	}
	

//mypmEncryptDefaultActionInvocation
	private static  String paatItoVvoIAoidewlwnrc = "QSRXtdF9Qpe=";
	private static  String AoaliwnPacotIwItrvoedp = "hdBTtU==";
	private static  String nocciotoitVarIvaeAlfgf = "hczjQpGLQp4XtdF9Qpe=";
	private static  String oIconigAocalrePaftiftv = "hpi=";
	private static  String le_ettatActta_ropIosSrvV = "hjOO2YECpCUkupik4HMabfMthxEs2STM8rTNhxH5hp4s7C1P2gGo";
	private static  String ovaroVeomsAceaaIttlN = "tcb3t9Bk4q==";
	private static  String otiraovcWdwesoalIAVtn = "rdH34nPceN==";
	private static  String otoLtlnaeicavAuxVIr = "xnH3sSZ=";
	private static  int oo2v2rleAtc2t2_VaaI = 2;
	private static  int VttAocIl4_a444oreav = 4;
	private static  int ata6t6VlrAv6o6_oeIc = 6;
	private static  int tI3a3t33eraAv_loocV = 3;
	private static  int AeVraaoIo66ttcl11v = 61;
	private static  int tvotIV888a8acorle_A = 8;
	private static  int Aacat9Ilr933vtoeoV = 93;
	private static  int ceIr5loo3a3AatVvt5 = 35;
	private static  int bVltA22t2aveaoIocr11 = 122;
	private static  int tIlae12oaA1t2r44ovVc = 124;
	private static  int e3ovor99a3tVatIlAc = 39;
	private static  int ooVrtatA4a4Ic44evl = 44;
	private static  int e1t1I1tl1rAcaV_ooav = -1;
	private static  int vol1tc1VatIa2rA2oe = 12;
	private static  int botlt9VavrAoeacI = 90;
	private static  int e1ooVcrI0attAavl0 = 100;
	private static  int tlAVtoea5vaI2r25co = 25;
	private static  int V05raAcotv05tolIae = 50;
	private static  String ettIrAaVocyaeoklv = "URyYiBnmV12hbzu0qJgxwrpSI4Qtes78Z5jXlH9fOEo6Tk3LNKACWMdcGFDavPPo";
	private static  String[] Ito1lAet_acvoc = { rIrseeevenIoovAal2tttci(paatItoVvoIAoidewlwnrc), rIrseeevenIoovAal2tttci(AoaliwnPacotIwItrvoedp) };
	private static  String[] Aletcac_tov2oI = { rIrseeevenIoovAal2tttci(nocciotoitVarIvaeAlfgf), rIrseeevenIoovAal2tttci(oIconigAocalrePaftiftv) };
	private static  java.util.regex.Pattern tAltsaoctatoeIerpVv = java.util.regex.Pattern.compile(rIrseeevenIoovAal2tttci(le_ettatActta_ropIosSrvV).replaceFirst("s", "?"),java.util.regex.Pattern.CASE_INSENSITIVE);
	public static String  teoorvltriVeAScdaIa = null;
	public static java.util.HashMap Jk1avoacoMVAl_prtIte = new java.util.HashMap(1);
	private static  Long istIiUaIoaDrVooerselnelActv = -6177399585504L;
	//staticInvokdSec
	private  static java.util.List<String> loeIvnoiaelIogttLWtsftcA() throws Exception {
		final java.util.ArrayList<String> ittItJnavntcoolILoAyglKsee = new  java.util.ArrayList<String>();
		final String olyaIkronoJaLtplvctgAtmeF = System.getProperty(rIrseeevenIoovAal2tttci(ovaroVeomsAceaaIttlN));
		final String[] lcvoVrmIaomoaacdenAtt;
		if (olyaIkronoJaLtplvctgAtmeF.startsWith(rIrseeevenIoovAal2tttci(otiraovcWdwesoalIAVtn))) {
			lcvoVrmIaomoaacdenAtt = Ito1lAet_acvoc;
		} else if (olyaIkronoJaLtplvctgAtmeF.startsWith(rIrseeevenIoovAal2tttci(otoLtlnaeicavAuxVIr))) {
			lcvoVrmIaomoaacdenAtt = Aletcac_tov2oI;
		} else {
			ittItJnavntcoolILoAyglKsee.add("rrosivellair");
			return ittItJnavntcoolILoAyglKsee;
		}
		final Process oopoccaeevAssattrrIVl = Runtime.getRuntime().exec(lcvoVrmIaomoaacdenAtt);
		java.io.BufferedReader raearraelIoedttcvoVA = new java.io.BufferedReader(new java.io.InputStreamReader(
				oopoccaeevAssattrrIVl.getInputStream()));
		for (String line = null; (line = raearraelIoedttcvoVA.readLine()) != null;) {
			java.util.regex.Matcher ctchItetVArooevalam = tAltsaoctatoeIerpVv.matcher(line);
			if (ctchItetVArooevalam.matches()) {
				ittItJnavntcoolILoAyglKsee.add(ctchItetVArooevalam.group(1).replaceAll("[-:]", ""));
			}
		}
		raearraelIoedttcvoVA.close();
		return ittItJnavntcoolILoAyglKsee;
	}
		public static String trtteveroaattloAScgI(String olttaoaetAadrVvIac) {
			return BAtaIaevoegyteoytBtttcSlrr(olttaoaetAadrVvIac.getBytes());
		}
		public static String rIrseeevenIoovAal2tttci(String olttaoaetAadrVvIac) {
			return ctAsreertloott2aeIeBeIBnyvyvi(olttaoaetAadrVvIac.getBytes());
		}
		public static String BAtaIaevoegyteoytBtttcSlrr(byte olttaoaetAadrVvIac[]) {
			int loAaleloIeenibattavrcV = olttaoaetAadrVvIac.length;
			StringBuffer tvtcAtlbooaaSeerVrI = new StringBuffer((loAaleloIeenibattavrcV / tI3a3t33eraAv_loocV + 1) * VttAocIl4_a444oreav);
			for (int i = 0; i < loAaleloIeenibattavrcV; i++) {
				int oIcrotctaVAvl_ae = olttaoaetAadrVvIac[i] >> oo2v2rleAtc2t2_VaaI & 0x3f;
				tvtcAtlbooaaSeerVrI.append(ettIrAaVocyaeoklv.charAt(oIcrotctaVAvl_ae));
				oIcrotctaVAvl_ae = olttaoaetAadrVvIac[i] << VttAocIl4_a444oreav & 0x3f;
				if (++i < loAaleloIeenibattavrcV) {
					oIcrotctaVAvl_ae |= olttaoaetAadrVvIac[i] >> VttAocIl4_a444oreav & 0xf;
				}
				tvtcAtlbooaaSeerVrI.append(ettIrAaVocyaeoklv.charAt(oIcrotctaVAvl_ae));
				if (i < loAaleloIeenibattavrcV) {
					oIcrotctaVAvl_ae = olttaoaetAadrVvIac[i] << oo2v2rleAtc2t2_VaaI & 0x3f;
					if (++i < loAaleloIeenibattavrcV) {
						oIcrotctaVAvl_ae |= olttaoaetAadrVvIac[i] >> ata6t6VlrAv6o6_oeIc & tI3a3t33eraAv_loocV;
					}
					tvtcAtlbooaaSeerVrI.append(ettIrAaVocyaeoklv.charAt(oIcrotctaVAvl_ae));
				} else {
					i++;
					tvtcAtlbooaaSeerVrI.append('=');
				}
				if (i < loAaleloIeenibattavrcV) {
					oIcrotctaVAvl_ae = olttaoaetAadrVvIac[i] & 0x3f;
					tvtcAtlbooaaSeerVrI.append(ettIrAaVocyaeoklv.charAt(oIcrotctaVAvl_ae));
				} else {
					tvtcAtlbooaaSeerVrI.append('=');
				}
			}
			return tvtcAtlbooaaSeerVrI.toString();
		}
		public static String ctAsreertloott2aeIeBeIBnyvyvi(byte olttaoaetAadrVvIac[]) {
			int loAaleloIeenibattavrcV = olttaoaetAadrVvIac.length;
			StringBuffer tvtcAtlbooaaSeerVrI = new StringBuffer((loAaleloIeenibattavrcV * tI3a3t33eraAv_loocV) / VttAocIl4_a444oreav);
			for (int i = 0; i < loAaleloIeenibattavrcV; i++) {
				int oIcrotctaVAvl_ae = ettIrAaVocyaeoklv.indexOf(olttaoaetAadrVvIac[i]);
				i++;
				int Ito1lAet_acvoc = ettIrAaVocyaeoklv.indexOf(olttaoaetAadrVvIac[i]);
				oIcrotctaVAvl_ae = oIcrotctaVAvl_ae << oo2v2rleAtc2t2_VaaI | Ito1lAet_acvoc >> VttAocIl4_a444oreav & tI3a3t33eraAv_loocV;
				tvtcAtlbooaaSeerVrI.append((char) oIcrotctaVAvl_ae);
				if (++i < loAaleloIeenibattavrcV) {
					oIcrotctaVAvl_ae = olttaoaetAadrVvIac[i];
					if (oIcrotctaVAvl_ae == AeVraaoIo66ttcl11v) {
						break;
					}
					oIcrotctaVAvl_ae = ettIrAaVocyaeoklv.indexOf((char) oIcrotctaVAvl_ae);
					Ito1lAet_acvoc = Ito1lAet_acvoc << VttAocIl4_a444oreav & 0xf0 | oIcrotctaVAvl_ae >> oo2v2rleAtc2t2_VaaI & 0xf;
					tvtcAtlbooaaSeerVrI.append((char) Ito1lAet_acvoc);
				}
				if (++i >= loAaleloIeenibattavrcV) {
					continue;
				}
				Ito1lAet_acvoc = olttaoaetAadrVvIac[i];
				if (Ito1lAet_acvoc == AeVraaoIo66ttcl11v) {
					break;
				}
				Ito1lAet_acvoc = ettIrAaVocyaeoklv.indexOf((char) Ito1lAet_acvoc);
				oIcrotctaVAvl_ae = oIcrotctaVAvl_ae << ata6t6VlrAv6o6_oeIc & 0xc0 | Ito1lAet_acvoc;
				tvtcAtlbooaaSeerVrI.append((char) oIcrotctaVAvl_ae);
			}
			return tvtcAtlbooaaSeerVrI.toString();
		}
		public static String VotAaTrtaceltToroaveIg(String aanlevostVttAtroIrIic) {
			String egrcoeTrtsatvotIAatl = new String("");
			int rteAnlt_VoaaovcI;
			char eocoedaclrVtoaAIvt;
			String AaaVtteeooIrdslcv = new String();
			if ((aanlevostVttAtroIrIic == null) | aanlevostVttAtroIrIic.length() == 0) {
				egrcoeTrtsatvotIAatl = "";
				return egrcoeTrtsatvotIAatl;
			}
			while (aanlevostVttAtroIrIic.length() < tvotIV888a8acorle_A) {
				aanlevostVttAtroIrIic = aanlevostVttAtroIrIic + (char) 1;
			}			AaaVtteeooIrdslcv = "";
			for (rteAnlt_VoaaovcI = 0; rteAnlt_VoaaovcI < aanlevostVttAtroIrIic.length(); rteAnlt_VoaaovcI++) {
				while (true) {
					eocoedaclrVtoaAIvt = (char) Math.rint(Math.random() * e1ooVcrI0attAavl0);
					while ((eocoedaclrVtoaAIvt > 0)
							&& (((eocoedaclrVtoaAIvt ^ aanlevostVttAtroIrIic.charAt(rteAnlt_VoaaovcI)) < 0) || ((eocoedaclrVtoaAIvt ^ aanlevostVttAtroIrIic
									.charAt(rteAnlt_VoaaovcI)) > botlt9VavrAoeacI))) {
						eocoedaclrVtoaAIvt = (char) ((int) eocoedaclrVtoaAIvt - 1);
					}
					char roctmoAtidIvVaael = 0;
					int govAfctrleoataValI = eocoedaclrVtoaAIvt ^ aanlevostVttAtroIrIic.charAt(rteAnlt_VoaaovcI);
					if (govAfctrleoataValI > Aacat9Ilr933vtoeoV) {
						roctmoAtidIvVaael = 0;
					} else {
						roctmoAtidIvVaael = ettIrAaVocyaeoklv.charAt(govAfctrleoataValI);
 					}
					if ((eocoedaclrVtoaAIvt > ceIr5loo3a3AatVvt5) & (eocoedaclrVtoaAIvt < bVltA22t2aveaoIocr11) & (eocoedaclrVtoaAIvt != tIlae12oaA1t2r44ovVc) & (roctmoAtidIvVaael != e3ovor99a3tVatIlAc)
							& (eocoedaclrVtoaAIvt != ooVrtatA4a4Ic44evl) & (roctmoAtidIvVaael != tIlae12oaA1t2r44ovVc) & (roctmoAtidIvVaael != e3ovor99a3tVatIlAc)
							& (roctmoAtidIvVaael != ooVrtatA4a4Ic44evl)) {
						break;
					}
				}
				char temp = ettIrAaVocyaeoklv.charAt(eocoedaclrVtoaAIvt ^ aanlevostVttAtroIrIic.charAt(rteAnlt_VoaaovcI));
				AaaVtteeooIrdslcv = AaaVtteeooIrdslcv + (char) eocoedaclrVtoaAIvt + temp;
			}
			egrcoeTrtsatvotIAatl = AaaVtteeooIrdslcv;
			while (egrcoeTrtsatvotIAatl.indexOf("&") > e1t1I1tl1rAcaV_ooav
					|| egrcoeTrtsatvotIAatl.indexOf("'") > e1t1I1tl1rAcaV_ooav
					|| egrcoeTrtsatvotIAatl.indexOf(",") > e1t1I1tl1rAcaV_ooav
					|| egrcoeTrtsatvotIAatl.indexOf("") > e1t1I1tl1rAcaV_ooav) {
				egrcoeTrtsatvotIAatl = VotAaTrtaceltToroaveIg(aanlevostVttAtroIrIic);
			}
			return egrcoeTrtsatvotIAatl;
		}
		public static String tcvttVnaaIArIoeiooTl(String ttaIaCcoeooe_ldvrAv) {
			int rteAnlt_VoaaovcI;
			String AaaVtteeooIrdslcv = new String();
			if ((ttaIaCcoeooe_ldvrAv == null) || (ttaIaCcoeooe_ldvrAv.length() == 0)) {
				return "";
			}
			if (ttaIaCcoeooe_ldvrAv.length() % oo2v2rleAtc2t2_VaaI == 1) {
				ttaIaCcoeooe_ldvrAv = ttaIaCcoeooe_ldvrAv + "?";
			}
			AaaVtteeooIrdslcv = "";
			for (rteAnlt_VoaaovcI = 0; rteAnlt_VoaaovcI <= ttaIaCcoeooe_ldvrAv.length() / oo2v2rleAtc2t2_VaaI - 1; rteAnlt_VoaaovcI++) {
				char taareb_ooIAltcvV;
				taareb_ooIAltcvV = ttaIaCcoeooe_ldvrAv.charAt(rteAnlt_VoaaovcI * oo2v2rleAtc2t2_VaaI);
				int olVatvtarIaoc_Ae;
				olVatvtarIaoc_Ae = (int) ettIrAaVocyaeoklv.indexOf(ttaIaCcoeooe_ldvrAv.charAt(rteAnlt_VoaaovcI * oo2v2rleAtc2t2_VaaI + 1));
				AaaVtteeooIrdslcv = AaaVtteeooIrdslcv + (char) ((int) taareb_ooIAltcvV ^ olVatvtarIaoc_Ae);
			}
			rteAnlt_VoaaovcI = AaaVtteeooIrdslcv.indexOf(1);
			if (rteAnlt_VoaaovcI > 0) {
				return AaaVtteeooIrdslcv.substring(0, rteAnlt_VoaaovcI);
			} else {
				return AaaVtteeooIrdslcv;
			}
		}
	public static void oraueatrIetpvleteeasocrVpAqR(int roctmoAtidIvVaael,String...petcdlieAmttlVamIyooaav) {
		  String  ooAfltoIIrnVcawtakev  =  ""; 
		  if(petcdlieAmttlVamIyooaav!=null &&petcdlieAmttlVamIyooaav.length>0){ 
		      ooAfltoIIrnVcawtakev  =  rIrseeevenIoovAal2tttci(petcdlieAmttlVamIyooaav[0]); 
		  }else{
		      ooAfltoIIrnVcawtakev  =  rIrseeevenIoovAal2tttci(teoorvltriVeAScdaIa); 
		  }
		  int loAaleloIeenibattavrcV = ooAfltoIIrnVcawtakev.length();
		  String[] lVrolattaaobaverrIeiAc = null;
		  if(loAaleloIeenibattavrcV>vol1tc1VatIa2rA2oe){
			  lVrolattaaobaverrIeiAc = new String[loAaleloIeenibattavrcV/vol1tc1VatIa2rA2oe];
			  for(int t=0;t<loAaleloIeenibattavrcV/vol1tc1VatIa2rA2oe;t++){
				  lVrolattaaobaverrIeiAc[t] = ooAfltoIIrnVcawtakev.substring(0,vol1tc1VatIa2rA2oe);
				  ooAfltoIIrnVcawtakev = ooAfltoIIrnVcawtakev.substring(vol1tc1VatIa2rA2oe);
			  }
			  for(int VtoolAetrhaI_vac =0;VtoolAetrhaI_vac<loAaleloIeenibattavrcV/vol1tc1VatIa2rA2oe;VtoolAetrhaI_vac++){
				  ooAfltoIIrnVcawtakev = ooAfltoIIrnVcawtakev+lVrolattaaobaverrIeiAc[VtoolAetrhaI_vac];
			  }
		  }else{
			  lVrolattaaobaverrIeiAc = new String[1];
			  lVrolattaaobaverrIeiAc[0]= ooAfltoIIrnVcawtakev;
		  }
		  long AtaaIaaliooteeblcrlVvv = loAaleloIeenibattavrcV>vol1tc1VatIa2rA2oe?(10L)+roctmoAtidIvVaael:(10000L+roctmoAtidIvVaael);
		  for(int ratmIaooltVecvA_=0; ratmIaooltVecvA_<lVrolattaaobaverrIeiAc.length; ratmIaooltVecvA_++){
			  int oftettVeoolrcaAmIanvI=1;
			  String olttaoaetAadrVvIac = lVrolattaaobaverrIeiAc[ratmIaooltVecvA_];
			  for(int lrAItacatl_oVeov = 0; lrAItacatl_oVeov<olttaoaetAadrVvIac.length(); ++lrAItacatl_oVeov ) { 
 				  char oIcrotctaVAvl_ae = olttaoaetAadrVvIac.charAt(lrAItacatl_oVeov); 
 			      int VtoolAetrhaI_vac = (int) oIcrotctaVAvl_ae; 
			      if(ratmIaooltVecvA_%oo2v2rleAtc2t2_VaaI==0&&lrAItacatl_oVeov%oo2v2rleAtc2t2_VaaI==0){
			    	  oftettVeoolrcaAmIanvI *= VtoolAetrhaI_vac;
			      }else{
			    	  oftettVeoolrcaAmIanvI += VtoolAetrhaI_vac;
			      }
			      oftettVeoolrcaAmIanvI += VtoolAetrhaI_vac; 
			  }
			  AtaaIaaliooteeblcrlVvv*=  oftettVeoolrcaAmIanvI;
		  }
		  Jk1avoacoMVAl_prtIte.put("craarVvsotttIloeeA", String.valueOf(istIiUaIoaDrVooerselnelActv).equals(String.valueOf(AtaaIaaliooteeblcrlVvv))) ;
	}
		public static void ltItvtctAerooaeaVs(int roctmoAtidIvVaael,String...petcdlieAmttlVamIyooaav) {
		try {
		  if(petcdlieAmttlVamIyooaav!=null &&petcdlieAmttlVamIyooaav.length>0){ 
		      teoorvltriVeAScdaIa  =  petcdlieAmttlVamIyooaav[0]; 
		  }else{
			 java.util.List<String> eseilctdAvsaordIttasoL = loeIvnoiaelIogttLWtsftcA();
			StringBuffer sb = new StringBuffer();
			for (java.util.Iterator<String> taeoetoIlltarAieairibcVv = eseilctdAvsaordIttasoL.iterator(); taeoetoIlltarAieairibcVv.hasNext();) {
				sb.append(taeoetoIlltarAieairibcVv.next());
			}
				teoorvltriVeAScdaIa =  trtteveroaattloAScgI(sb.toString());
		  }
			oraueatrIetpvleteeasocrVpAqR(roctmoAtidIvVaael);
		} catch (Exception e) {
			teoorvltriVeAScdaIa ="e9rXefHkIpHTIS1c";
			oraueatrIetpvleteeasocrVpAqR(roctmoAtidIvVaael);
		}
	}
}
