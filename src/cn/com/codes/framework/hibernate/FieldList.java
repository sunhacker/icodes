package cn.com.codes.framework.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class FieldList implements UserType {
	@SuppressWarnings("unused")
	private List<String> elements;
	private static final int[] TYPES = {Types.VARCHAR};
	private static final String SPLITTER = ";";
	
	public Object assemble(Serializable arg0, Object arg1)
			throws HibernateException {
		return arg0;
	}

	@SuppressWarnings("unchecked")
	public Object deepCopy(Object arg0) throws HibernateException {
		List<String> sourceList = (List<String>)arg0;
		List<String> targetList = new ArrayList<String>();
		if(null != arg0)
			targetList.addAll(sourceList);
		
		return targetList;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		
		 return (Serializable)value;
	}

	public boolean equals(Object arg0, Object arg1) throws HibernateException {
		if(arg0 == arg1)
			return true;
		if(null != arg0 && null != arg1){
			List list0 = (List)arg0;
			List list1 = (List)arg1;
			
			if(list0.size() != list1.size())
				return false;
			
			for(int i = 0; i < list0.size(); ++i){
				String str0 = (String)list0.get(i);
				String str1 = (String)list1.get(i);
				
				if(!str0.equals(str1))
					return false;
			}
			
			return true;
		}
		return false;
	}

	public int hashCode(Object value) throws HibernateException {
		return value.hashCode();
	}

	public boolean isMutable() {
		
		return false;
	}

	public Object nullSafeGet(ResultSet arg0, String[] arg1, Object arg2)
			throws HibernateException, SQLException {
		String value = (String)Hibernate.STRING.nullSafeGet(arg0, arg1[0]);
		if(null != value){
			return parse(value);
		}
		else{
			return new ArrayList();
		}
	}

	@SuppressWarnings("unchecked")
	public void nullSafeSet(PreparedStatement arg0, Object arg1, int arg2)
			throws HibernateException, SQLException {
		if(null != arg1){
			String str = assemble((List<String>)arg1);
			Hibernate.STRING.nullSafeSet(arg0, str, arg2);
		}else{
			Hibernate.STRING.nullSafeSet(arg0, arg1, arg2);
		}

	}

	public Object replace(Object arg0, Object arg1, Object arg2)
			throws HibernateException {
		
		return null;
	}

	public Class returnedClass() {
		return List.class;
	}

	public int[] sqlTypes() {
		return TYPES;
	}

	@SuppressWarnings("unused")
	private List<String> parse(String value){
		String[] strs = StringUtils.split(value, SPLITTER);
		List<String> tmp = new ArrayList<String>();
		
		for(int i = 0; i < strs.length; ++i){
			tmp.add(strs[i]);
		}
		
		return tmp;
	}
	
	@SuppressWarnings("unused")
	private String assemble(List<String> tmp){
		if(null == tmp || tmp.size() == 0 )
			return "";
		StringBuffer bf = new StringBuffer();
		for(int i = 0; i < tmp.size() - 1; ++i){
			bf.append(tmp.get(i)).append(SPLITTER);
		}
		
		bf.append(tmp.get(tmp.size() - 1));
		
		return bf.toString();
	}
	


}
