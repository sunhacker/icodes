package cn.com.codes.framework.security;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.framework.security.Button;

/**
 * 
 * @author liuyg
 *initFunctionContainer 方法以后要修改,己标注
 */
public class ButtonContainerService {
	
	private static Logger logger = Logger
			.getLogger(HibernateGenericController.class);
	protected static Map<String, List<Button>> container = new HashMap<String, List<Button>>();
	public static Map<String, List<Button>> functionContainer = new HashMap<String, List<Button>>();
	
	private static String sendAddress;
	
	private static String ipAddress;
	private static String macAddress;
	protected static String imagesDirec ;


	public HibernateGenericController hibernateGenericController;
	

	public ButtonContainerService() {
	
	}

	public ButtonContainerService(
			HibernateGenericController hibernateGenericController) {

		this.hibernateGenericController = hibernateGenericController;
	
		

	}

  

	
	private static String hexByte(byte b) {
		String s = "000000" + Integer.toHexString(b);
		return s.substring(s.length() - 2);
	}
	private void loadReptViewCount(){
		
	}
	

	private void sort(Map<String, List<Button>> container ){
		Iterator it = container.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, List<Button>> me = 	(Map.Entry<String, List<Button>>)it.next() ;
			List<Button> list = me.getValue();
			Button[] buttons = new Button[list.size()];
			java.util.Arrays.sort(list.toArray(buttons), new ButtonComparator());
			list.clear();
			for(Button but :buttons){
				list.add(but);
			}
			container.put(me.getKey(), list);
		}
		
	}

	public void initFunctionContainer() {

		StringBuffer sql = new StringBuffer();
		sql.append("select    BUTTONKEY||'.'||FUNCTIONNAME as button  from  T_FUNCTION f where methods is not null  and BUTTONKEY is not null and isleaf =1 order by seq");
		
		List<Object> list = hibernateGenericController.findBySql(
				sql.toString(), null);
		for (Object result : list) {
			String buttonUrl = result.toString();
			String buttonKey = buttonUrl.substring(0, buttonUrl
					.lastIndexOf("."));
			String buttonName = buttonUrl
					.substring(buttonUrl.lastIndexOf(".") + 1);
			if (functionContainer.containsKey(buttonKey)) {
				List<Button> buttonlist = functionContainer.get(buttonKey);
				if(!buttonlist.contains(new Button(buttonName.trim()))){
					buttonlist.add(new Button(buttonName.trim()));
				}
				
			} else {
				List<Button> buttonlist = new ArrayList<Button>();
				if(!buttonlist.contains(new Button(buttonName.trim()))){
					buttonlist.add(new Button(buttonName.trim()));
				}
				functionContainer.put(buttonKey, buttonlist);
			}
		}
	}
	//仅做测试用
	public void drawButton(String page){
		StringBuffer sb = new StringBuffer();
		sb.append("var pmBar = new dhtmlXToolbarObject(\"toolbarObj\");\n");
		sb.append("pmBar.setIconsPath(\"../dhtmlx/toolbar/images/\");\n");
		List<Button> buttonlist = container.get(page);
		int i = 1;
		for(Button button : buttonlist){
			if(button.isShare()){
				sb.append("pmBar.addButton(\""+button.getId()+"\","+ i+" , \"\", \""+button.getIcon()+"\");\n");
				sb.append("pmBar.setItemToolTip(\""+button.getId()+"\", \""+button.getName()+"\");\n");	
				i++;
			}else{
				List<Button> fbuttonlist = functionContainer.get(page);
				if(fbuttonlist.contains(new Button(button.getName()))){
					sb.append("pmBar.addButton(\""+button.getId()+"\","+ i+" , \"\", \""+button.getIcon()+"\");\n");
					sb.append("pmBar.setItemToolTip(\""+button.getId()+"\", \""+button.getName()+"\");\n");	
					i++;
				}
			}
		}
		System.out.println(sb.toString());
	}

	public static void main(String[] args) {

		ButtonContainerService c = new ButtonContainerService();
		Iterator it = container.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
			String buttonKey = (String) me.getKey();
			List<Button> list = (List<Button>) me.getValue();
			System.out.println("function button key=" + buttonKey);
			for (Button button : list) {
				System.out.println(button);
			}
		}
	}
	
	class ButtonComparator implements Comparator{
		
	    public int compare(Object o1, Object o2) {
	        Integer key1;
	        Integer key2;
	        if (o1 instanceof Button) {
	            key1 = ((Button)o1).getSeq();
	        }else{
	        	key1 = o1.hashCode();
	        }
	        if (o2 instanceof Button) {
	            key2 =((Button)o2).getSeq();
	        }else{
	        	key2 = o2.hashCode();
	        }
	        return key1.compareTo(key2);
	    }
	}

	public String getImagesDirec() {
		return imagesDirec;
	}

	public void setImagesDirec(String imagesDirec) {
		this.imagesDirec = imagesDirec;
	}

	public static String getSendAddress() {
		if(sendAddress==null||sendAddress.endsWith("")||sendAddress.indexOf("@")<0){
			sendAddress = "liuygneusoft@163.com";
		}
		return sendAddress;
	}

	public static String getIpAddress() {
		return ipAddress;
	}

	public static String getMacAddress() {
		return macAddress;
	}


}
