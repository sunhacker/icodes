package cn.com.codes.framework.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.framework.security.filter.SecurityContextHolder;

public class ButtonTag extends TagSupport {

	private Log logger = LogFactory.getLog(ButtonTag.class);
	private static Map navigatorMap = new HashMap();
	private static HibernateGenericController hibernateGenericController;
	private String page;
	private String pagination = "true";
	private String filter = "false";
	private String hidden = "false";
	private String find = "false";
	private String checkAll = "false";
	private String imgPath = "";
	private String sw = "false";
	private String reFresh = "true";
	private String reFreshHdl = "true";
	private String back = "true";
	private String backHdl = "true";
	private String setHome = "false";
	private String hand = "true";
	static{
		navigatorMap.put("role", "角色列表");
		navigatorMap.put("userManager/user", "用户列表");
		navigatorMap.put("userManager/group", "用户组列表");
		navigatorMap.put("outLineManager", "测试需求分解列表");
		navigatorMap.put("bugManager", "项目缺陷管理");
		navigatorMap.put("caseManager", "测试用例列表");
		navigatorMap.put("msgManager", "消息管理列表");
		navigatorMap.put("testBaseSet", "测试基础数据设置");
		navigatorMap.put("singleTestTask", "测试项目列表");
		navigatorMap.put("flwSet", "测试项目 测试流程设置");
		navigatorMap.put("swTestTaskList", "测试项目切换选择列表");
		navigatorMap.put("sendMsgManager", "发布消息");	
		navigatorMap.put("allMySpr", "缺陷管理");
		//navigatorMap.put("batchAssgin", "批量分配视图");
		
		
	}
	
	public int doStartTag() throws JspException {

		return SKIP_BODY;
	}



	/**
	 * @deprecated
	 */
	private  List getUserPrivilege(String userId){
		
		StringBuffer sql = new StringBuffer();
		sql.append("select  distinct f.SECURITY_URL from ");
		sql.append("   T_FUNCTION f ,");
		sql.append("  (select distinct rf.FUNCTIONID from ");
		sql.append("  T_ROLE_FUNCTION_REAL rf ,");
		sql.append("  (select ur.ROLEID from ");
		sql.append("    T_USER_ROLE_REAL ur ,");
		sql.append("   T_USER  u");
		sql.append("   where u.ID='" + userId + "' and "
				+ "ur.USERID=u.ID)  myrole");
		sql.append("   where rf.ROLEID= myrole.ROLEID )  myfunction ");
		sql.append("    where f.FUNCTIONID=myfunction.FUNCTIONID AND f.ISLEAF=1 AND f.SECURITY_URL is not null ");
		if (logger.isInfoEnabled()) {
			logger.info(sql.toString());
		}
		List<Object> powerList = getDbdelegate().findBySql(sql
				.toString(), null);
		return powerList;
	}
	private HibernateGenericController getDbdelegate() {
		if (hibernateGenericController == null) {
			hibernateGenericController = (HibernateGenericController) WebApplicationContextUtils
					.getWebApplicationContext(pageContext.getServletContext())
					.getBean("hibernateGenericController");
		}
		return hibernateGenericController;
	}
	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPagination() {
		return pagination;
	}

	public void setPagination(String pagination) {
		this.pagination = pagination;
	}

	public String getHidden() {
		return hidden;
	}

	public void setHidden(String hidden) {
		this.hidden = hidden;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getFind() {
		return find;
	}

	public void setFind(String find) {
		this.find = find;
	}

	public String getCheckAll() {
		return checkAll;
	}

	public void setCheckAll(String checkAll) {
		this.checkAll = checkAll;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getSw() {
		return sw;
	}

	public void setSw(String sw) {
		this.sw = sw;
	}

	public String getReFresh() {
		return reFresh;
	}

	public void setReFresh(String refresh) {
		this.reFresh = refresh;
	}

	public String getBack() {
		return back;
	}

	public void setBack(String back) {
		this.back = back;
	}

	public String getReFreshHdl() {
		return reFreshHdl;
	}

	public void setReFreshHdl(String reFreshHdl) {
		this.reFreshHdl = reFreshHdl;
	}

	public String getBackHdl() {
		return backHdl;
	}

	public void setBackHdl(String backHdl) {
		this.backHdl = backHdl;
	}

	public String getSetHome() {
		return setHome;
	}

	public void setSetHome(String setHome) {
		this.setHome = setHome;
	}

	public String getHand() {
		return hand;
	}

	public void setHand(String hand) {
		this.hand = hand;
	}
	

}
