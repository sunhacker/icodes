var image;
$(function () {
	$.parser.parse();
	initEnvList();
})

function initEnvList() {
	$("#envListTab").xdatagrid({
		url: baseUrl + '/env/envAction!getImageList.action',
		method: 'post',
		height: mainObjs.tableHeight,
		pagination: true,
		emptyMsg:"无数据",
		checkOnSelect:true,
		selectOnCheck:true,
		pageList:[10,20,30],
		pageNumber: 1,
		pageSize: 10,
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
		   // {field:'imageId',title:'选择',checkbox:true,align:'center'},
			{field:'imageName',title:'镜像名称',width:"10%",align:'center'},
			{field:'fileType',title:'镜像类型',width:"10%",align:'center', formatter: imageTypeFormatter},
			{field:'imageDesc',title:'镜像描述',width:"15%",align:'center'},
			{field:'createDate',title:'创建时间',width:"15%",align:'center'},
			{field:'serverIp',title:'docker容器IP',width:"15%",align:'center'},
			{field:'uploader',title:'上传人',width:"10%",align:'center'},
			{field:'appName',title:'已部署应用',width:"10%",align:'center'},
			{field:'appStatus',title:'应用状态',width:"10%",align:'center'},
			{field:'dfdas',title:'操作',width:"15%",align:'center', formatter: operationFormatter}
			
		]]
	});
	
	
}

function operationFormatter(value,row,index) {
	//"<a href='javascript:void(0)' onclick=\"showDeployAppWin('"+ row.imageName +"','"+ row.tag +"')\">部署应用</a>";
	var html = "";
	if (row.imageId != "" && row.imageId != null) {
		html = "<a href='javascript:void(0)' onclick=\"showDeployAppWin('"+ row.imageName +"','"+ row.tag +"','"+ row.id +"','"+ row.serverIp +"')\">部署应用</a>";
	} else {
		html = "<a href='javascript:void(0)' onclick=\"showBuildImageWin('"+ row.imageName +"','"+ row.remarks +"','"+ row.id +"','"+ row.fileType +"','"+ row.imageDesc +"','"+ row.uploader +"','"+ row.filePath +"','"+ row.serverIp +"','"+ row.imageFileName +"','"+ row.tag +"')\">构建镜像</a>";
		html += "&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"deleteImage('"+ row.id +"','"+ row.imageFileName +"')\">删除镜像</a>";
	}
	if (row.appId != "" && row.appId != null) {
		if (row.appStatus == "running") {
			html = "<a href='javascript:void(0)' onclick=\"stopApp('"+ row.appId +"','"+ row.serverIp +"','"+ row.id +"')\">停止应用</a>";
			html += "&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"showDeployAppDetailWin('"+ row.imageName +"','"+ row.tag +"','"+ row.appName +"','"+ row.appAccessIp +"','"+ row.appPort +"','"+ row.containersPort +"','"+ row.appEnvVar +"','"+ row.appLinkContainer +"','"+ row.serverIp +"')\">应用详情</a>";
		} else if (row.appStatus == "created" || row.appStatus == "exited") {
			html = "<a href='javascript:void(0)' onclick=\"startApp('"+ row.appId +"','"+ row.serverIp +"','"+ row.id +"')\">启动应用</a>";
			html += "&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"showDeployAppDetailWin('"+ row.imageName +"','"+ row.tag +"','"+ row.appName +"','"+ row.appAccessIp +"','"+ row.appPort +"','"+ row.containersPort +"','"+ row.appEnvVar +"','"+ row.appLinkContainer +"','"+ row.serverIp +"')\">应用详情</a>";
		}
	}
	return html;
}

function imageTypeFormatter(value,row,index) {
	if (value == "1") {
		return "tar";
	}
	if (value == "2") {
		return "Dockerfile";
	}
}

function certManager() {
	$("#uploadTarImageBtn").hide();
	$("#uploadDockerfileBtn").hide();
	$("#certManagerBtn").hide();
	$("#returnEnvBtn").show();
	$("#uploadCertBtn").show();
	$("#envListTab").parents("div.datagrid").css("display","none");
	$("#certManagerListTab").xdatagrid({
		url: baseUrl + '/env/envAction!getCertList.action',
		method: 'post',
		height: mainObjs.tableHeight,
		pagination: true,
		emptyMsg:"无数据",
		checkOnSelect:true,
		selectOnCheck:true,
		pageList:[10,20,30],
		pageNumber: 1,
		pageSize: 10,
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
		   // {field:'imageId',title:'选择',checkbox:true,align:'center'},
			{field:'serverIp',title:'docker容器IP',width:"25%",align:'center'},
			{field:'certName',title:'证书名称',width:"30%",align:'center'},
			{field:'uploadDate',title:'上传时间',width:"25%",align:'center'},
			{field:'ddd',title:'操作',width:"20%",align:'center', formatter: certOperationFormatter}
			
		]]
	});
}

function returnEnv() {
	$("#uploadTarImageBtn").show();
	$("#uploadDockerfileBtn").show();
	$("#certManagerBtn").show();
	$("#returnEnvBtn").hide();
	$("#uploadCertBtn").hide();
	$("#certManagerListTab").parents("div.datagrid").css("display","none");
	$("#envListTab").parents("div.datagrid").css("display","block");
}

function certOperationFormatter(value,row,index) {
	return "<a href='javascript:void(0)' onclick='deleteCert("+ JSON.stringify(row) +")'>删除</a>";
}

function deleteCert(row) {
	$.xconfirm({
		msg:'您确定要删除证书吗?',
		okFn: function() {
			var url = baseUrl + "/env/envAction!deleteCert.action"; 
			$.ajax({
				type: "post",
				dataType: "json",
				url: url,
				data: {
					'dto.cert.id': row.id,
					'dto.cert.serverIp': row.serverIp,
					'dto.cert.certName': row.certName,
					'dto.cert.apiCertName': row.apiCertName,
					'dto.cert.certPwd': row.certPwd,
					'dto.cert.uploadDate': row.uploadDate
				},
				success: function(data) {
					if (data.oprateResult == "success") {
						$.xalert({title:'提示', msg: '删除证书成功！'});
					} else {
						$.xalert({title:'提示', msg: '删除证书失败！'});
					}
					$('#certManagerListTab').datagrid('reload');
				}
			});
		}
	});
	
}

function deleteImage(id, imageFileName) {
	$.xconfirm({
		msg:'您确定要删除镜像吗?',
		okFn: function() {
			var url = baseUrl + "/env/envAction!deleteImage.action"; 
			$.ajax({
				type: "post",
				dataType: "json",
				url: url,
				data: {
					'dto.image.id': id,
					'dto.image.imageFileName': imageFileName
				},
				success: function(data) {
					if (data.oprateResult == "success") {
						$.xalert({title:'提示', msg: '删除镜像文件成功！'});
					} else {
						$.xalert({title:'提示', msg: '删除镜像文件失败！'});
					}
					$('#envListTab').datagrid('reload');
				}
			});
		}
	});
}

function showDeployAppDetailWin(imageName, tag, appName, appAccessIp, appPort, containersPort, appEnvVar, appLinkContainer, serverIp) {
	$("#deployAppForm input").attr("disabled", "disabled");
	$("#deployAppForm input[id=imageName]").textbox('setValue',imageName + ":" + tag);
	$("#deployAppForm input[id=appName]").textbox('setValue',appName);
	if (appAccessIp != "" && appAccessIp != "null") {
		$("#deployAppForm input[id=hostIp]").textbox('setValue',appAccessIp);
	}
	if (containersPort != "" && containersPort != "null") {
		$("#deployAppForm input[id=containersPort]").textbox('setValue',containersPort);
	}
	if (appPort != "" && appPort != "null") {
		$("#deployAppForm input[id=hostPort]").textbox('setValue',appPort);
	}
	if (appEnvVar != "" && appEnvVar != "null") {
		$("#deployAppForm input[id=envVar]").textbox('setValue',appEnvVar);
	}
	if (serverIp != "" && serverIp != "null") {
		//$("#deployAppForm input[id=serverIp]").textbox('setValue',serverIp);
		$(".dCertServerIp").xcombobox("setValue",serverIp);
	}
	if (appLinkContainer != "" && appLinkContainer != "null") {
		$("#deployAppForm input[id=link]").textbox('setValue',appLinkContainer);
	}
	$("#dApiTr").css("display", "none");
	$("#deployAppForm input[name=dnServerIp]").parents("span.textbox").css("display","none");
	$("#deployAppFooter").show();
	$("#deployAppWin").parent().css("border","none");
	$("#deployAppWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#deployAppWin").xwindow('setTitle','应用详情').xwindow('open');
	$("#deployAppFooter").hide();
}

function showBuildImageWin(imageName, remarks, id, fileType, imageDesc, uploader, filePath, serverIp,imageFileName, tag) {
	$("#buildImageForm").xform('clear');
	$("#buildImageForm input[id=id]").val(id);
	$("#buildImageForm input[id=fileType]").val(fileType);
	$("#buildImageForm input[id=uploader]").val(uploader);
	$("#buildImageForm input[id=imageName]").textbox('setValue',imageName);
	$("#buildImageForm input[id=serverIp]").textbox('setValue',serverIp);
	$("#buildImageForm input[id=imageDesc]").textbox('setValue',imageDesc);
	$("#buildImageForm input[id=imageRemarks]").textbox('setValue',remarks);
	$("#buildImageForm input[id=imageFile]").textbox('setValue',filePath);
	$("#buildImageForm input[id=imageFileName]").val(imageFileName);
	$("#buildImageForm input[id=imageTag]").val(tag);
	$("#buildImageWin").parent().css("border","none");
	$("#buildImageForm input[id=buildnoApiCert]").prop("checked" , true);
	$("#byServerIp").next().css("display","none");
	$("#bnServerIp").next().css("display","block");
	$("#bnServerIp").textbox({width: "100%"});
	$("#buildImageFooter").show();
	$("#buildImageWin input[id=bnServerIp]").textbox({required:true});
	$("#buildImageWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#buildImageWin").xwindow('setTitle','构建镜像').xwindow('open');
}

function buildImage() {
	var valid = $("#buildImageForm").xform('validate');
	if (!valid) {
		return;
	}
	var api = $("#buildImageForm input[name='apiCert']:checked").val();
	var serverIp = $("#buildImageForm input[id=bnServerIp]").val();
	if (api == "1") {
		serverIp = $("#buildImageForm input[id=byServerIp]").val();
	}
	var id = $("#buildImageForm input[id=id]").val();
	var fileType = $("#buildImageForm input[id=fileType]").val();
	var uploader = $("#buildImageForm input[id=uploader]").val();
	var imageName = $("#buildImageForm input[id=imageName]").val();
	var imageDesc = $("#buildImageForm input[id=imageDesc]").val();
	var imageRemarks = $("#buildImageForm input[id=imageRemarks]").val();
	var filePath = $("#buildImageForm input[id=imageFile]").val();
	var imageFileName = $("#buildImageForm input[id=imageFileName]").val();
	var iamgeTag = $("#buildImageForm input[id=imageTag]").val();
	var url = baseUrl + "/env/envAction!buildTarImage.action";
	if (fileType == "2") {
		url = baseUrl + "/env/envAction!buildDockerfileImage.action"; 
	}
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.id': id,
			'dto.image.fileType': fileType,
			'dto.image.filePath': filePath,
			'dto.image.imageFileName': imageFileName,
			'dto.image.tag': iamgeTag,
			'dto.image.serverIp': serverIp,
			'dto.image.imageDesc': imageDesc,
			'dto.image.remarks': imageRemarks,
			'dto.image.uploader': uploader,
			'dto.image.imageName': imageName
		},
		success: function(data) {
			if (data.oprateResult == "success") {
				$.xalert({title:'提示', msg: '构建镜像成功！'});
				$('#envListTab').datagrid('reload');
				$("#buildImageWin").xwindow('close');
			} else if (data.oprateResult == "noCert") {
				$.xalert({title:'提示', msg: '请先上传API证书！'});
				$("#buildImageWin").xwindow('close');
			}else {
				$.xalert({title:'提示', msg: '构建镜像失败！'});
			}
			
		}
	});
}

function startApp(appId, serverIp, id) {
	var url = baseUrl + "/env/envAction!startApp.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.appId': appId,
			'dto.image.serverIp': serverIp,
			'dto.image.id': id
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$('#envListTab').datagrid('reload');
			} else if (data.oprateResult == "noCert") {
				$.xalert({title:'提示', msg: '请先上传API证书！'});
			} else {
				$.xalert({title:'提示', msg: '启动应用时发生错误！'});
			}
			
		}
	});
}

function stopApp(appId, serverIp, id) {
	var url = baseUrl + "/env/envAction!stopApp.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.appId': appId,
			'dto.image.serverIp': serverIp,
			'dto.image.id': id
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$('#envListTab').datagrid('reload');
			} else if (data.oprateResult == "noCert") {
				$.xalert({title:'提示', msg: '请先上传API证书！'});
			} else {
				$.xalert({title:'提示', msg: '停止应用时发生错误！'});
			}
			
		}
	});
}

function showDeployAppWin(imageName, tag, id, serverIp) {
	$("#deployAppForm").xform('clear');
	$("#deployAppForm input").removeAttr("disabled");
	$("#deployAppWin").parent().css("border","none");
	$("#deployAppWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#deployAppFooter").show();
	$("#deployAppWin").xwindow('setTitle','部署应用').xwindow('open');
	$("#tid").val(id);
	$("#deployAppForm input[id=deploynoApiCert]").prop("checked" , true);
	$("#dyServerIp").next().css("display","none");
	$("#dnServerIp").next().css("display","block");
	$("#dnServerIp").textbox({width: "100%"});
	$("#deployAppForm input[id=dnServerIp]").textbox({required:true});
	$("#deployAppForm input[id=imageName]").textbox("setValue" , imageName + ":" + tag);
	$("#deployAppForm input[id=serverIp]").textbox('setValue',serverIp);
}

function deployApp() {
	var url = baseUrl + "/env/envAction!deployApp.action";
	var api = $("#deployAppForm input[name='dApiCert']:checked").val();
	var serverIp = $("#deployAppForm input[id=dnServerIp]").val();
	if (api == "1") {
		serverIp = $("#deployAppForm input[id=dyServerIp]").val();
	}
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.deployApp.imageName': $("#deployAppForm input[id=imageName]").val(),
			'dto.deployApp.appName': $("#deployAppForm input[id=appName]").val(),
			'dto.deployApp.hostIp': $("#deployAppForm input[id=hostIp]").val(),
			'dto.deployApp.containersPort': $("#deployAppForm input[id=containersPort]").val(),
			'dto.deployApp.hostPort': $("#deployAppForm input[id=hostPort]").val(),
			'dto.deployApp.envVar': $("#deployAppForm input[id=envVar]").val(),
			'dto.deployApp.serverIp': serverIp,//$("#deployAppForm input[id=serverIp]").val()
			'dto.deployApp.link': $("#deployAppForm input[id=link]").val(),
			'dto.image.id':$("#tid").val()
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$("#deployAppForm").xform('clear');
				$("#deployAppWin").xwindow('close');
				$('#envListTab').datagrid('reload');
			} else if (data.oprateResult == "noCert") {
				$.xalert({title:'提示', msg: '请先上传API证书！'});
				$("#deployAppForm").xform('clear');
				$("#deployAppWin").xwindow('close');
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
			
		}
	});
}

function showUploadImageWin() {
	$("#uploadImageForm").xform('clear');
	$("#uploadImageFooter").show();
	$("#uploadImageWin").parent().css("border","none");
	$("#uploadImageWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#uploadImageWin").xwindow('setTitle','上传镜像').xwindow('open');
	initUploadImage();
}

function showUploadDockerfileWin() {
	$("#uploadDockerfileForm").xform('clear');
	$("#uploadDockerfileFooter").show();
	$("#uploadDockerfileWin").parent().css("border","none");
	$("#uploadDockerfileWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#uploadDockerfileWin").xwindow('setTitle','构建镜像').xwindow('open');
	initBuildImage();
}

function dockerfileChange(obj) {
	$("#dockerfileField").val($(obj).val());
}

function uploadBuildImageFile() {
	if(!$(".file-caption-name").val()){
		$.xalert({title:'提示',msg:'请选择需要上传的dockerfile 文件！'});
		return;
	}
	var valid = $("#uploadDockerfileForm").xform('validate');
	if (!valid) {
		return;
	}
	var file = $("#buildImageFileId").val();
	var len = file.indexOf(".tar.gz")
	if (len <= 0) {
		$.xalert({title:'提示', msg: '镜像文件必须是tar.gz文件类型！'});
		return;
	} 
	$("#buildImageFileId").fileinput("upload");
	
}

function saveBuildImageInfo(filePath, imageFileName) {
	var url = baseUrl + "/env/envAction!saveImage.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.imageName': $("#uploadDockerfileForm input[id=imageName]").val(),
			//'dto.image.tag': image.tag,
			'dto.image.imageDesc': $("#uploadDockerfileForm input[id=imageDesc]").val(),
			'dto.image.remarks': $("#uploadDockerfileForm input[id=imageRemarks]").val(),
			//'dto.image.createDate': image.createDate,
			'dto.image.filePath': filePath,
			'dto.image.imageFileName': imageFileName,
			'dto.image.fileType': '2',
			'dto.image.uploader': $("#loginName").text()
			//'dto.image.serverIp': $("#uploadDockerfileForm input[id=serverIp]").val()
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$("#uploadDockerfileForm").xform('clear');
				$("#uploadDockerfileWin").xwindow('close');
				$('#envListTab').datagrid('reload');
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
			
		}
	});
}

function initBuildImage(){
	$("#buildImageFileId").fileinput('reset');
	$("#buildImageFileId").fileinput('destroy');
	$("#buildImageFileId").off('filebatchuploadsuccess');
	$("#buildImageFileId").fileinput({
		theme: 'fa',
		language:'zh',
		uploadUrl:baseUrl + "/buildImage?envEnableXss=false", // you must set a valid URL here else you will get an error
		deleteUrl:'',
		allowedFileExtensions: ['tar.gz'],
		showPreview:false,
		overwriteInitial: false,
		uploadAsync:false,
		autoReplace: true,
//		showUploadedThumbs:false,
		maxFileSize: 2000000,
		maxFileCount: 1,
		imageMaxWidth : 200,
		imageMaxHeight : 100,
		enctype: 'multipart/form-data',
		showRemove:false,
		showClose:false,
		showUpload:false,
		allowedPreviewTypes:['tar.gz'],
		dropZoneEnabled:false,
		uploadExtraData:function() {
			var data = {
					serverIp: $("#uploadDockerfileForm input[id=serverIp]").val()
			};
			return data;
		},
		fileActionSettings:{
	        showUpload: false,
	        //showRemove:true,
//			url: baseUrl + "/uploader?type=del",// 删除url 
	    },
		slugCallback: function (filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on("filebatchselected", function(event, files) {
//		if(files!=null && files.length>0){
//			$(this).fileinput("upload");
//		}
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		var returnData = data.response;
		if (returnData.oprateResult == "success") {
			saveBuildImageInfo(returnData.filePath, returnData.imageFileName);
		} else {
			$.xalert({title:'提示', msg: '上传镜像文件失败！'});
		}
		setTimeout(function(){
			$(".kv-upload-progress").hide();
		}, 2000);
	}).on('filebatchuploaderror', function(event, data,  previewId, index) {
	}).on('fileloaded', function(event, data,  previewId, index) {
		var filesFrames = $('#buildImageFileId').fileinput('getFrames');
		if(filesFrames.length>1){
			$.xalert({title:'提示',msg:'最多上传5个文件！'});
			$("#"+previewId).remove();
			return;
		}
		
		
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
	});
}

function imagefileChange(obj) {
	$("#imagefileField").val($(obj).val());
}

function initUploadImage(){
	$("#uploadImageFileId").fileinput('reset');
	$("#uploadImageFileId").fileinput('destroy');
	$("#uploadImageFileId").off('filebatchuploadsuccess');
	$("#uploadImageFileId").fileinput({
		theme: 'fa',
		language:'zh',
		uploadUrl:baseUrl + "/uploadImage?envEnableXss=false", // you must set a valid URL here else you will get an error
		deleteUrl:'',
		allowedFileExtensions:['tar'],
		showPreview:false,
		overwriteInitial: false,
		uploadAsync:false,
		autoReplace: true,
//		showUploadedThumbs:false,
		maxFileSize: 2000000,
		maxFileCount: 1,
		imageMaxWidth : 200,
		imageMaxHeight : 100,
		enctype: 'multipart/form-data',
		showRemove:false,
		showClose:false,
		showUpload:false,
		allowedPreviewTypes:['tar'],
		dropZoneEnabled:false,
		uploadExtraData:function() {
			var data = {
					serverIp: $("#uploadImageForm input[id=serverIp]").val()
			};
			return data;
		},
		fileActionSettings:{
	        showUpload: false,
	       // showRemove:true,
//			url: baseUrl + "/uploader?type=del",// 删除url 
	    },
		slugCallback: function (filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on("filebatchselected", function(event, files) {
//		if(files!=null && files.length>0){
//			$(this).fileinput("upload");
//		}
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		//image = data.response;
		var returnData = data.response;
		if (returnData.oprateResult == "success") {
			saveUploadImageInfo(returnData.filePath, returnData.imageFileName);
		} else {
			$.xalert({title:'提示', msg: '上传镜像文件失败！'});
		}
		setTimeout(function(){
			$(".kv-upload-progress").hide();
		}, 2000);
	}).on('filebatchuploaderror', function(event, data,  previewId, index) {
	}).on('fileloaded', function(event, data,  previewId, index) {
		var filesFrames = $('#uploadImageFileId').fileinput('getFrames');
		if(filesFrames.length>1){
			$.xalert({title:'提示',msg:'最多上传1个文件！'});
			$("#"+previewId).remove();
			return;
		}
		
		
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
	});
}

//上传镜像
function uploadTarImageFile() {
	if(!$(".file-caption-name").val()){
		$.xalert({title:'提示',msg:'请选择需要上传的tar文件！'});
		return;
	}
	var valid = $("#uploadImageForm").xform('validate');
	if (!valid) {
		return;
	}
	var file = $("#uploadImageFileId").val();
	var len = file.indexOf(".tar")
	if (len <= 0) {
		$.xalert({title:'提示', msg: '镜像文件必须是tar文件类型！'});
		return;
	} 
	//var file = $("#uploadImageFileId").val();
	var startIndex = file.lastIndexOf(".")
	if (startIndex != -1) {
		var fileType = file.substring(startIndex+1,startIndex.length);
		if (fileType != "tar") {
			$.xalert({title:'提示', msg: '镜像文件必须是tar文件类型！'});
			return;
		}
	}
	$("#uploadImageFileId").fileinput("upload");
	/*
	var url = baseUrl + "/env/envAction!saveImage.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.imageId': image.imageId,
			'dto.image.imageName': image.imageName,
			'dto.image.tag': image.tag,
			'dto.image.imageDesc': $("#uploadImageForm input[id=imageDesc]").val(),
			'dto.image.remarks': $("#uploadImageForm input[id=imageRemarks]").val(),
			'dto.image.createDate': image.createDate,
			'dto.image.uploader': $("#loginName").text(),
			'dto.image.serverIp': $("#uploadImageForm input[id=serverIp]").val()
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$("#uploadImageForm").xform('clear');
				$("#uploadImageWin").xwindow('close');
				$('#envListTab').datagrid('reload');
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
			
		}
	});*/
	
}

function saveUploadImageInfo(filePath, imageFileName) {
	var url = baseUrl + "/env/envAction!saveImage.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.image.imageName': $("#uploadImageForm input[id=imageName]").val(),
			//'dto.image.tag': image.tag,
			'dto.image.imageDesc': $("#uploadImageForm input[id=imageDesc]").val(),
			'dto.image.remarks': $("#uploadImageForm input[id=imageRemarks]").val(),
			//'dto.image.createDate': image.createDate,
			'dto.image.filePath': filePath,
			'dto.image.imageFileName': imageFileName,
			'dto.image.fileType': '1',
			'dto.image.uploader': $("#loginName").text()
			//'dto.image.serverIp': $("#uploadImageForm input[id=serverIp]").val()
		},
		success: function(data) {
			//console.log("data:" + JSON.stringify(data))
			if (data.oprateResult == "success") {
				$("#uploadImageForm").xform('clear');
				$("#uploadImageWin").xwindow('close');
				$('#envListTab').datagrid('reload');
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
			
		}
	});
}

function showUploadCertWin() {
	$("#uploadCertForm").xform('clear');
	$("#uploadCertFooter").show();
	$("#uploadCertWin").parent().css("border","none");
	$("#uploadCertWin").prev().css({ color: "#ffff", background: "#101010" });
	$("#uploadCertWin").xwindow('setTitle','上传证书').xwindow('open');
	initUploadCert();
}

function uploadCertFile() {
	if(!$(".file-caption-name").val()){
		$.xalert({title:'提示',msg:'请选择需要上传的证书文件！'});
		return;
	}
	var valid = $("#uploadCertForm").xform('validate');
	if (!valid) {
		return;
	}
	$("#uploadCertFileId").fileinput("upload");
	
}

function saveUploadCertInfo(certName, apiCertName) {
	var url = baseUrl + "/env/envAction!saveCert.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		data: {
			'dto.cert.serverIp': $("#uploadCertForm input[id=serverIp]").val(),
			'dto.cert.certName': certName,
			'dto.cert.apiCertName': apiCertName,
			'dto.cert.certPwd': $("#uploadCertForm input[id=certPwd]").val()
		},
		success: function(data) {
			if (data.oprateResult == "success") {
				$("#uploadCertForm").xform('clear');
				$("#uploadCertWin").xwindow('close');
				$('#certManagerListTab').datagrid('reload');
			} else {
				$.xalert({title:'提示', msg: '保存数据时发生错误！'});
			}
			
		}
	});
}

function initUploadCert(){
	$("#uploadCertFileId").fileinput('reset');
	$("#uploadCertFileId").fileinput('destroy');
	$("#uploadCertFileId").off('filebatchuploadsuccess');
	$("#uploadCertFileId").fileinput({
		theme: 'fa',
		language:'zh',
		uploadUrl:baseUrl + "/uploadCert?envEnableXss=false", // you must set a valid URL here else you will get an error
		deleteUrl:'',
//		allowedFileExtensions: ['jpg', 'png', 'gif','bmp','ppt','pptx','txt','pdf','doc','docx','object'],
		showPreview:false,
		overwriteInitial: false,
		uploadAsync:false,
		autoReplace: true,
//		showUploadedThumbs:false,
		maxFileSize: 2000000,
		maxFileCount: 1,
		imageMaxWidth : 200,
		imageMaxHeight : 100,
		enctype: 'multipart/form-data',
		showRemove:false,
		showClose:false,
		showUpload:false,
		allowedPreviewTypes:['image', 'html', 'text', 'video', 'pdf', 'flash','object'],
		dropZoneEnabled:false,
		fileActionSettings:{
	        showUpload: false,
	        //showRemove:true,
//			url: baseUrl + "/uploader?type=del",// 删除url 
	    },
		slugCallback: function (filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on("filebatchselected", function(event, files) {
//		if(files!=null && files.length>0){
//			$(this).fileinput("upload");
//		}
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		var returnData = data.response;
		if (returnData.oprateResult == "success") {
			saveUploadCertInfo(returnData.certName, returnData.apiCertName);
		} else {
			$.xalert({title:'提示', msg: '上传镜像文件失败！'});
		}
		setTimeout(function(){
			$(".kv-upload-progress").hide();
		}, 2000);
	}).on('filebatchuploaderror', function(event, data,  previewId, index) {
	}).on('fileloaded', function(event, data,  previewId, index) {
		var filesFrames = $('#uploadCertFileId').fileinput('getFrames');
		if(filesFrames.length>1){
			$.xalert({title:'提示',msg:'最多上传1个文件！'});
			$("#"+previewId).remove();
			return;
		}
		
		
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
	});
}

function buildYApiCert() {
	$("#byServerIp").next().css("display","block");
	$("#bnServerIp").css("display","none");
	$("#buildImageForm input[id=bnServerIp]").textbox({required:false});
	$("#buildImageForm input[name=bnServerIp]").parents("span.textbox").css("display","none")
	var url = baseUrl + "/env/envAction!getCertList.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		success: function(data) {
			$(".bCertServerIp").xcombobox({
				data:data.rows
			});
			var row = data.rows;
			$(".bCertServerIp").xcombobox("setValue",row[0]);
			
		}
	});
	
}
function buildNoApiCert() {
	$("#byServerIp").next().css("display","none");
	$("#bnServerIp").css("display","none");
	$("#buildImageForm input[id=bnServerIp]").textbox({required:true});
	$("#buildImageForm input[name=bnServerIp]").parents("span.textbox").css("display","block")
}

function deployYApiCert() {
	$("#dyServerIp").next().css("display","block");
	$("#dnServerIp").css("display","none");
	$("#deployAppForm input[id=dnServerIp]").textbox({required:false});
	//$("#dyServerIp").next().combobox({required:true});
	$("#deployAppForm input[name=dnServerIp]").parents("span.textbox").css("display","none")
	var url = baseUrl + "/env/envAction!getCertList.action";
	$.ajax({
		type: "post",
		dataType: "json",
		url: url,
		success: function(data) {
			$(".dCertServerIp").xcombobox({
				data:data.rows
			});
			var row = data.rows;
			$(".dCertServerIp").xcombobox("setValue",row[0]);
		}
	});
	
}
function deployNoApiCert() {
	$("#dyServerIp").next().css("display","none");
	$("#dnServerIp").css("display","none");
	$("#deployAppForm input[id=dnServerIp]").textbox({required:true});
	//$("#deployAppForm input[id=dyServerIp]").next().combobox({required:false});
	//$("#dyServerIp").next().next("span.textbox").css("display","none");
	$("#deployAppForm input[name=dnServerIp]").parents("span.textbox").css("display","block");
}
//# sourceURL=envManager.js