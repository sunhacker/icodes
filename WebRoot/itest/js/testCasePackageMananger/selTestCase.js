var packageId = '';
var taskId="";
var currNodeId="";
var rootNodeId;
var switchFlag=0;
var loadCount=0;
var originSelTestCasedId = [];
var backupSelTestCasedId = []; //用于和初始的选择用例比较

//获取页面url参数
function getQueryParam(name) {
       var obj = $('#testCaseListDlg').dialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

$(function(){
	$.parser.parse();
    taskId = $("#taksIdmain").val();
	packageId = getQueryParam('testCasePackageId');
	//获取已选择的用例id
	getSelTestCasedIds();
	caseTree(packageId);
	
	//初始化下拉框查询条件
	loadTestCaseCategory();
	loadTestCasePriority();

});

//测试需求
function caseTree(){
	$('#selCaseTree').xtree({
	    url:baseUrl+'/caseManager/caseManagerAction!loadTree.action?dto.taskId='+taskId+'&dto.command=',
	    method:'get',
		animate:true,
		lines:true,
//		dnd:true,
		onLoadSuccess:function(node,data){  
			var mid="";
			if(node==null){
				currNodeId=rootNodeId = data[0].id;
				if(loadCount==0){
					getCaseList(data[0].id);
					loadCount=1;
				}
				var nodeDep = $('#selCaseTree').xtree('find',data[0].id);  
				if (null != nodeDep && undefined != nodeDep){  
					$('#selCaseTree').xtree('select',nodeDep.target);  
				}  
				$(nodeDep.target).tooltip({
				    position: 'right',
				    content: '<span style="color:#fff">root节点测试需求，不能增加测试用例.</span>',
				    hideDelay:100,
				    onShow: function(){
						$(this).tooltip('tip').css({
							backgroundColor: '#666',
							borderColor: '#666'
						});
				    }
				});
				mid = data[0].id;
				if(data[0].children!=null){
					for ( var it in data[0].children) {
						mid+='_'+data[0].children[it].id;
					}
				}
				
			}else{
				var nodeDep = $('#selCaseTree').xtree('find',node.id);  
				if (null != nodeDep && undefined != nodeDep){  
					$('#selCaseTree').xtree('select',nodeDep.target);  
				}  
				
				for ( var it in data) {
					mid+='_'+data[it].id;
				}
			}
			
			if(switchFlag==1){
				$.get(
					baseUrl + "/caseManager/caseManagerAction!loadNodedetalData.action",
					{'dto.remark': 'onlyNormal',
					 'dto.countStr':mid
					},
					function(data,status,xhr){
						if(status=='success'){
							for ( var c in data) {
								var node = $('#selCaseTree').xtree('find', data[c].moduleId);
								var text =  '('+data[c].caseCount+'/'+data[c].scrpCount+')';
								$('#selCaseTree').xtree('update', {
									target: node.target,
									text: '<span title="斜杠前后为用例数和bug数">'+node.text+text+'</span>'
								});
							}
						}else{
							$.xalert({title:'提示',msg:'系统错误！'});
						}
					},
				"json");
			}
			
			//获取bug信息
		/*	caseCountInfo();*/
		 }, 
		onClick: function(node){
			currNodeId = node.id;
			getCaseList(node.id);
			/*caseCountInfo();*/
		},
		onContextMenu:function(e,node){
			e.preventDefault();
			$(this).xtree('select', node.target);
		}
	});
}
//测试列表
function getCaseList(currNodeId){
	$("#selCaseList").xdatagrid({
		url: baseUrl + '/caseManager/caseManagerAction!loadCaseList.action?dto.command=simple'+'&dto.taskId='+taskId+'&dto.currNodeId='+currNodeId+'&dto.weightFlag=1',
		method: 'get',
		height: mainObjs.tableHeight-140,
		fitColumns:true,
		singleSelect:false,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		idField:'testCaseId',
	    layout:['list','first','prev','manual', "sep",'next','last','refresh','info'],
		pageList: [10,30,50,100],
		showPageList:true,
		columns:[[
			{field:'',title:'选择',checkbox:true,align:'center'},
			{field:'testCaseId',title:'编号',width:'5%',align:'center'},
			{field:'taskName',title:'项目名称',width:'10%',align:'center',halign:'center'},
			{field:'testCaseDes',title:'用例描述',width:'30%',align:'left',halign:'center',formatter:caseDetail},
			{field:'testStatus',title:'最新状态',width:'10%',align:'center',formatter:testStatusFormat},
			{field:'typeName',title:'类别',width:'5%',align:'left',halign:'center'},
			{field:'priName',title:'优先级',width:'10%',align:'center'},
			{field:'auditerNmae',title:'最近处理人',width:'12%',align:'center'},
			{field:'authorName',title:'编写人',width:'10%',align:'center',formatter:caseHistory},
			{field:'weight',title:'成本',width:'5%',align:'center'},
			{field:'creatdate',title:'编写日期',width:'12%',align:'left'}
		]],
		onLoadSuccess : function (data) {								
			if (data.total==0) {
				$('#selCaseList').parent().find(".datagrid-view2 .datagrid-body").append('<div style="font-size:16px; text-align: center;">暂无数据</div>');
			}
			checkedSelectedTestCase();

		},
		onSelect: function (rowIndex, rowData){
			var tCaseId = rowData.testCaseId;
			if(!originSelTestCasedId.includes(tCaseId)){
				originSelTestCasedId.push(tCaseId)
			}
		},
		onUnselect: function (rowIndex, rowData) {
			var testCaseId = rowData.testCaseId;
			var index = originSelTestCasedId.indexOf(testCaseId);
			if(index>=0){
				originSelTestCasedId.splice(index,1);
			}
		},
		onSelectAll: function (rows){
			if(rows.length > 0){
				rows.map(function(value,index){
					if(!originSelTestCasedId.includes(value.testCaseId)){
						originSelTestCasedId.push(value.testCaseId);
					}
				});
			}
		},
		onUnselectAll: function (rows) {
			if(rows.length > 0){
				rows.map(function(value,index){
					if(originSelTestCasedId.includes(value.testCaseId)){
						var findIndex = originSelTestCasedId.indexOf(value.testCaseId);
						originSelTestCasedId.splice(findIndex,1);
					}
				});
			}
		}
		
	});
}
//用例状态
function testStatusFormat(value,row,index) {
	switch (value) {
	case 0:
		return '待审核';
	case 1:
		return '未测试';
	case 2:
		return '通过';
	case 3:
		return '未通过';
	case 4:
		return '不适用';
	case 5:
		return '阻塞';
	case 6:
		return '待修正';
	default:
		return '-';
	}
}

//用例详情
function caseDetail(value,row,index) {
	return "<a style=\"cursor: default;color:#000\" title=\"用例详情：--"+value+"\" href=\"javascript:;\">" + value + "</a>";
}
//用例历史
function caseHistory(value,row,index) {
	if(value=="" || value=="null" ||value==null){
		value=="--";
	}
	return "<span style=\"cursor: default\" title=\"" + value + "\" href=\"javascript:;\" >" + value + "</span>";
}

function operaTypeFormat(value,row,index){
	switch (value) {
	case 0:
		return '待审核';
	case 1:
		return '未测试';
	case 2:
		return '修改';
	case 3:
		return '未通过';
	case 4:
		return '执行';
	case 5:
		return '阻塞';
	case 6:
		return '待修正';
	default:
		return '-';
	}
}

//查找已经被选中的用例
function getSelTestCasedIds(){
	$.ajaxSettings.async = false;
	$.get(
			baseUrl +  '/testCasePkgManager/testCasePackageAction!getSelTestCasesByPkgId.action',
			{"dto.testCasePackage.packageId" : packageId},
			function(data,status,xhr){
				if(data.length > 0){
					data.map(function(value,index){
						originSelTestCasedId.push(value.testCaseId);
						//用于比较用户选择的用例是否变化
						backupSelTestCasedId.push(value.testCaseId);
					});
				}
			},"json");
}

//显示关联列表时如果有已选的关联用例，则显示选中关联用例
function checkedSelectedTestCase() {
	$.get(
			baseUrl +  '/testCasePkgManager/testCasePackageAction!getSelTestCasesByPkgId.action',
			{"dto.testCasePackage.packageId" : packageId},
			function(data,status,xhr){
				if(data.length > 0){
					data.map(function(value,index){
						$("#selCaseList").xdatagrid("selectRecord",value.testCaseId);
					});
				}
			},"json");

}

function submitSelTestCase(){
	var isOperationFlag = false;  //true: 有选择新的用例; false: 没有的新的用例
	if(backupSelTestCasedId.length == originSelTestCasedId.length){
		for(var i=0;i<originSelTestCasedId.length; i++){
			for(var j=0;j<backupSelTestCasedId.length;j++){
				if(!originSelTestCasedId.includes(backupSelTestCasedId[j])){
					isOperationFlag = true
					break;
				}
			}
			if(isOperationFlag) {
				break;
			}
		}
	}else{
		isOperationFlag = true
	}
	
	if(isOperationFlag){
		$.post(
				baseUrl + "/testCasePkgManager/testCasePackageAction!saveTestCase_CasePkg.action",
				{"dto.selectedTestCaseIds": originSelTestCasedId.toString(),
				 "dto.testCasePackage.packageId":packageId},
				function(dataObj) {
					if(dataObj.indexOf("success") === -1){
						$.xalert({title:'提示',msg:'保存失败，请稍后再试！'});
					}else{
						$.xalert({title:'提示',msg:'保存成功'});
						casePkgObjs.$testCasePkgTb.xdatagrid('reload');
						$('#testCaseListDlg').dialog('destroy');
					}
				},"text"
			);
	}else{
		$('#testCaseListDlg').dialog('destroy');
	}

}

//加载用例类别列表
function loadTestCaseCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 80,
				"dto.subName": "用例类型",
				"dto.flag": "1"
			},
			function(dat) {
				if (dat != null) {
					dat.rows = [{typeId:'',typeName:'全部'}].concat(dat.rows);
					$('#testCaseCategory').xcombobox({
						data:dat.rows,
						valueField:'typeId',
					    textField:'typeName',
					    prompt:'-请选择-',
					    onSelect:function(record){
					    	var testCasePriority = $("#testCasePriority").xcombobox("getValue");
					    	var dataParam ={}
					    	if(testCasePriority != null && testCasePriority != ''){					    		
					    		 dataParam['dto.testCaseInfo.priId'] = testCasePriority;
					    	}
					    	if(record.typeId != null && record.typeId != ''){					    		
					    		 dataParam['dto.testCaseInfo.caseTypeId'] = record.typeId;
					    	}
					    	$("#selCaseList").xdatagrid('reload',dataParam);
					    }
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载优先级列表
function loadTestCasePriority(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 80,
				"dto.subName": "用例优先级",
				"dto.flag": "1"
			},
			function(dat) {
				if (dat != null) {
					dat.rows = [{typeId:'',typeName:'全部'}].concat(dat.rows);
					$('#testCasePriority').xcombobox({
						data:dat.rows,
					    valueField:'typeId',
					    textField:'typeName',
					    prompt:'-请选择-',
					    onSelect:function(record){
					    	var testCaseCategory = $("#testCaseCategory").xcombobox("getValue");
                            var dataParam = {};                               
					    	if(testCaseCategory != null && testCaseCategory != ''){					    		
					    		 dataParam['dto.testCaseInfo.caseTypeId'] = testCaseCategory;
					    	}
					    	if(record.typeId != null && record.typeId != ''){					    		
					    		 dataParam['dto.testCaseInfo.priId'] = record.typeId;
					    	}
					    	$("#selCaseList").xdatagrid('reload',dataParam);
					    }
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}

function closeSelTestCaseWin(){
	 $('#testCaseListDlg').dialog('destroy');
}

//@ sourceURL=selTestCase.js