var casePkgObjs = {
	$testCasePkgTb:$("#testCasePkgTb"),
	$addOrEditPkgWin:$("#addOrEditPkgWin")
  
};
var currAccountId = $("#accountId").html();
var onselectF = false;
var oncheckF  = false;
var hshshss = "0";
var pckId = "";
var xmindurl = "";
var noReadlod =  1;
$(function(){
	$.parser.parse();
	getTestCasePkgManagerAuth();
	var currTaksId = $("#taksIdmain").val();
	var accountId = $('#accountId').text();
	//初始化角色列表
	casePkgObjs.$testCasePkgTb.xdatagrid({
		url:  baseUrl + '/testCasePkgManager/testCasePackageAction!loadTestCasePackageList.action',
		method: 'post',
		height:  mainObjs.tableHeight,
		singleSelect:true,
		checkOnSelect:true,
		selectOnCheck:true,
		queryParams:{'dto.testCasePackage.taskId':currTaksId,'dto.selectedUserIds':accountId},
		columns:[[
			{field:'packageId',checkbox:true},
			{field:'packageName',title:'测试包名称',width:'16%',height:'50px',align:'center',halign:'center'},
			{field:'execEnvironment',title:'执行环境',width:'7%',height:'50px',align:'center'},
			{field:'executor',title:'执行人',width:'14%',height:'50px',align:'center',halign:'center',formatter:executorFormat},
			{field:'expectedStartTime',title:'预计开始时间',width:'9%',height:'50px',align:'center',formatter:function(value){
				if(value){
					return value.split(" ")[0]
				}
			}},
			{field:'expectedEndTime',title:'预计结束时间',width:'9%',height:'50px',align:'center',formatter:function(value){
				if(value){
					return value.split(" ")[0]
				}
			}},
			{field:'actualStartTime',title:'实际开始时间',width:'9%',height:'50px',align:'center',formatter:function(value){
				if(value){
					return value.split(" ")[0]
				}
			}},
			{field:'exeCount',title:'执行率',width:'6%',height:'50px',align:'center',formatter:function(value,row,index){
                    if(!value){
                    	value=0
                    }
                    if(!row.notExeCount){
                    	row.notExeCount=0
                    }
                    
                    sumCase = value + row.notExeCount;
                    
                    if(!sumCase){
                    	return '-'
                    }
					return value + '/' + sumCase
			}},
			{field:'operator',title:'操作',width:'30.4%',height:'50px',align:'center',formatter:operatFormat},
			{field:'testCaseNames',hidden:true},
			{field:'notExeCount',hidden:true}
		]],
		onLoadSuccess : function (data) {								
			if (data.total==0) {
				$('#testCasePkgTb').parent().find(".datagrid-view2 .datagrid-body").append('<div style="font-size:16px; text-align: center;">暂无数据</div>');
			}
		}
	});
	
	 loadPeopleLists();
	 noReadlod = 0;
});

function loadTestCasePkgTb(){
	
}

//获取当前用户的权限
function getTestCasePkgManagerAuth(){
	var controlButton = $('button[schkUrl]');
	$.each(controlButton,function(i,n){
		var controId = controlButton[i].id;
		var controlUrl = $(controlButton[i]).attr('schkUrl');
		if(privilegeMap[controlUrl]!="1"){
			$("#"+controId).hide(); 
		}
	});
}

//'执行人'列
function executorFormat(value,row,index){
	var contentStr = "-";
	if(value){
		contentStr = (value.length<10)? value: "<span title='" + value + "'>" + value.substring(0,10) + "...</span>";
	}
	return contentStr;
}

//"操作"列
function operatFormat(value,row,index){
	var isContainCurrUser = false; //当前登录这是否是测试用例包的执行人  false:不是
	var columnStr = "<div>" +
    "<a type='button' style='cursor:pointer; padding:2px 5px!important;margin: 5px 11px 5px 0;color:#1e7cfb' onclick='selTestCase(\""+ row.packageId + "\",\"" + row.packageName + "\")' >" +
    "分配用例</a>";
	var viewBtnStr =  "<a type='button' style='cursor:pointer;padding:2px 4px!important;margin: 5px 0 5px 0;color:#1e7cfb'  onclick='viewTestCase(\""+ row.packageId + "\",\"" + row.packageName + "\")' >" +
    "查看用例</a>";
	var execBtnStr =  "<a type='button'  style='cursor:pointer;padding:2px 4px!important;margin: 5px 0 5px 0;color:#1e7cfb' onclick='executeTestCase(\""+ row.packageId + "\",\"" + row.packageName + "\")' >" +
    "执行用例</a>";
	if(null != row.userTestCasePkgs){
		var userIdsArr = row.userTestCasePkgs;
		for(var i=0;i<userIdsArr.length;i++){
			if(userIdsArr[i].userId === currAccountId){
				isContainCurrUser = true;
				break;
			} 
		}
	}
	
	columnStr += isContainCurrUser? execBtnStr:viewBtnStr;
	
	columnStr += "<a type='button' style='cursor:pointer;padding:2px 5px!important;margin: 5px 0 5px 10px;color:#1e7cfb'  onclick='viewTestCaseResult(\""+ row.packageId + "\",\"" + row.packageName + "\")'>" +
    "查看结果</a>";
	columnStr +="<a type='button'  style='cursor:pointer;padding:2px 5px!important;margin: 5px 0 5px 4px;color:#1e7cfb' onclick='viewHistoryRecord(\""+ row.packageId + "\",\"" + row.packageName + "\")' >" +
    "历史记录</a></div>";
	return columnStr;
}

//加载可分配人列表
function loadPeopleLists(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					var peopleList = dat.rows;

					var accountId = $('#accountId').text();
					var opti = '<option value="all">-全部人员-</option>';
					if(dat.rows.length > 0){
						for(var i=0;i<dat.rows.length;i++){
							if(accountId ==dat.rows[i].id ){
								opti = opti + '<option value="'+dat.rows[i].id+'" selected = "selected">'+dat.rows[i].name+'</option>';

							}else{								
								opti = opti + '<option value="'+dat.rows[i].id+'">'+dat.rows[i].name+'</option>';
							}
						}
					}
					$("#peopleNm").html(opti);
					$("#peopleNm").off().on("change",function(){
						var currTaksId = $("#taksIdmain").val();
						var queryParam = document.getElementById("queryParam").value;
				    	var peopleId = document.getElementById("peopleNm").value;
				    	var dataParam = {
				    			'dto.testCasePackage.taskId':currTaksId,
				    			'dto.selectedUserIds':peopleId
				    	}
				    	if(queryParam){
				    		dataParam['dto.queryParam']=queryParam
				    	}
				    	//模糊查询
				    	if(noReadlod==0){
				    		casePkgObjs.$testCasePkgTb.xdatagrid('reload',dataParam);
				    	}
						
					});
					$('#peopleNm').searchableSelect();
				} else {
					$.xalert("系统错误！", {type:'warning'});
				}
			}, "json");
}

//显示新增测试包弹窗
document.getElementById("showAddCasePkgWin").addEventListener('click',function(){

		 $("<div></div>").xdialog({
		    	id:'addOrEditCasePackageDlg',
		    	title: "新增测试包",
		    	width : 580,
		        height : 500,
		    	modal:true,
		    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!addOrEditCasePkg.action',
		    	queryParams: { "addOrEditPkgFlag": 'add' },
		        onClose : function() {
		        	casePkgObjs.$testCasePkgTb.xdatagrid('reload');
		            $(this).dialog('destroy');
		        }
		    });
	
});

//显示修改测试包弹窗
document.getElementById("showEditCasePkgWin").addEventListener('click',function(){
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	if (!row) {
		$.xalert('请选择要修改的一条记录', {type:'warning'});
		return;
	}

		 $("<div></div>").xdialog({
		    	id:'addOrEditCasePackageDlg',
		    	title: "修改测试包",
		    	width : 580,
		        height : 500,
		    	modal:true,
		    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!addOrEditCasePkg.action',
		    	queryParams: { "addOrEditPkgFlag": 'edit' },
		        onClose : function() {
		        	casePkgObjs.$testCasePkgTb.xdatagrid('reload');
		            $(this).dialog('destroy');
		        }
		    });
	
});



//删除选中的数据
document.getElementById("delCasePkg").addEventListener('click',function(){
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	if (!row) {
		$.xalert('请选择要删除的一条记录', {type:'warning'});
		return;
	}
	
	$.xconfirm({
		msg:'您确定删除选择的记录吗?',
		okFn: function() {
			$.post(
					baseUrl + '/testCasePkgManager/testCasePackageAction!deleteTestCasePkgById.action',
					{'dto.testCasePackage.packageId': row.packageId},
				function(data) {
					if (data.indexOf("success")>=0) {
						casePkgObjs.$testCasePkgTb.xdatagrid('reload');
						$.xalert('删除成功');
					} else {
						$.xalert("删除失败，请稍后再试", {type:'warning'});
					}
				},"text"
			);
		}
	});
});


function selTestCase(packageId,pkgName){
	 $("<div></div>").xdialog({
	    	id:'testCaseListDlg',
	    	title:pkgName +"测试包--关联用例",
	    	width : 1300,
	        height : document.documentElement.offsetHeight,
	    	modal:true,
	    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!selTestCase.action',
	    	queryParams: { "testCasePackageId": packageId },
	        onClose : function() {
	        	casePkgObjs.$testCasePkgTb.xdatagrid('reload');
	            $(this).dialog('destroy');
	        }
	    });
}

function viewTestCase(packageId,pkgName){
	
	 $("<div></div>").xdialog({
	    	id:'viewTestCaseDlg',
	    	title:pkgName +"测试包--查看用例",
	    	width : 1300,
	        height : 600,
	    	modal:true,
	    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!viewTestCase.action',
	    	queryParams: { "testCasePackageId": packageId},
	        onClose : function() {
	            $(this).dialog('destroy');
	        }
	    });
}

function executeTestCase(packageId,pkgName){
	
	 $("<div></div>").xdialog({
	    	id:'executeTestCaseDlg',
	    	title:pkgName +"测试包--执行用例",
	    	width : 1300,
	        height : document.documentElement.offsetHeight,
	    	modal:true,
	    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!executeTestCase.action',
	    	queryParams: { "testCasePackageId": packageId,'testCasePackageName':pkgName,'pageSource':'testCasePackage'},
	        onClose : function() {
	            $(this).dialog('destroy');
	            casePkgObjs.$testCasePkgTb.xdatagrid('reload');
	            
	            //执行管理子页面中的弹出框需要手动删除，否则会导致重复出现id=addOrEditFoot的div
               /* var executeWin = document.getElementById('addOrEditFoot').parentNode;
                document.body.removeChild(executeWin);*/
	        }
	    });
}

function viewTestCaseResult(packageId,pkgName){
	 $("<div></div>").xdialog({
	    	id:'viewTestCaseResultDlg',
	    	title:pkgName +"测试包--查看结果",
	    	width : 880,
	        height : 300,
	    	modal:true,
	    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!viewTestCaseResult.action',
	    	queryParams: { "testCasePackageId": packageId},
	        onClose : function() {
	            $(this).dialog('destroy');
	        }
	    });

}

function viewHistoryRecord(packageId,pkgName){
	$("<div></div>").xdialog({
    	id:'viewHistoryRecordDlg',
    	title:pkgName +"测试包--历史记录",
    	width : 1000,
        height : 700,
    	modal:true,
    	href:baseUrl + '/testCasePkgManager/testCasePackageAction!viewHistoryRecord.action',
        queryParams: { "testCasePackageId": packageId},
        onClose : function() {
            $(this).dialog('destroy');
        }
    });

}


document.getElementById('testcasepkgTool').onkeydown=function(event) {
	var e = event || window.event || arguments.callee.caller.arguments[0]; 
    if (e && e.keyCode == 13) {//keyCode=13是回车键；数字不同代表监听的按键不同
    	var currTaksId = $("#taksIdmain").val();
    	var queryParam = document.getElementById("queryParam").value;
    	var peopleId = document.getElementById("peopleNm").value;
    	var dataParam = {
    			'dto.testCasePackage.taskId':currTaksId,
    			'dto.selectedUserIds':peopleId
    	}
    	if(queryParam){
    		dataParam['dto.queryParam']=queryParam;
    	}
    	//模糊查询
    	casePkgObjs.$testCasePkgTb.xdatagrid('reload',dataParam);
    }
};


//打开导入弹窗
function upload(){
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	if (!row) {
		$.xalert('请选择要导入测试用例的测试包', {type:'warning'});
		return;
	}
	pckId = row.packageId;
	$('#uploadFoot').css('display','block');
	$("#uploadWin").xwindow('setTitle','导入').xwindow('open');
}

function exportCase(obj){
	
	var row = casePkgObjs.$testCasePkgTb.xdatagrid('getSelected');
	if (!row) {
		$.xalert('请选择要导出测试用例的测试包', {type:'warning'});
		return;
	}
	pckId = row.packageId;	
	var href = baseUrl + '/impExpMgr/caseImpExpAction!expCase4Imp.action?dto.pckId='+pckId;
	$(obj).prop('href', href);
}
//关闭导入弹窗
function closeUploadWin(data){
	$("#uploadWin").xwindow('close');
	if(data.split("<pre>")[1].split("</pre>")[0] != "success"){
		$.xalert({title:'提示',msg:data.split("<pre>")[1].split("</pre>")[0]});
	}else{
		casePkgObjs.$testCasePkgTb.xdatagrid('reload');
		$.xalert({title:'提示',msg:"导入成功！"});
	}
}

//打开从excel导入测试用例弹窗    
function uploadFromExcel(){
	hshshss = "0";
	//alert(fileType=="xmind");
	$("#uploadForm input").val("");
	$('#uploadFromExcelFoot').css('display','block');
	$("#uploadFromExcelWin").xwindow('setTitle','导入').xwindow('open');
	$("#importFile").fileinput("destroy");
	$("#importFile").fileinput({
		theme: 'fa',
		language:'zh',
		uploadUrl:baseUrl + '/impExpMgr/caseImpAction!impCase.action?dto.pckId='+pckId, // you must set a valid URL here else you will get an error  ['xlsx', 'xls']
		//uploadUrl:baseUrl + '/impExpMgr/caseImpAction!impCase.action', // you must set a valid URL here else you will get an error
		allowedFileExtensions: ['xlsx', 'xls', 'xmind'],
		showPreview:false,
		showClose:false,
		overwriteInitial: true,
		uploadAsync:false,
		autoReplace: true,
		showUploadedThumbs:false,
		maxFileSize: 6000,
		maxFileCount:1,
		imageMaxWidth : 200,
		imageMaxHeight : 100,
		enctype: 'multipart/form-data',
		//msgFilesTooMany :"选择图片超过了最大数量", 
		showRemove:false,
		showClose:false,
		showUpload:false,
		showDownload: false,
		allowedPreviewTypes: ['xlsx', 'xls', 'xmind'],
		dropZoneEnabled:false,
		initialPreviewAsData: false,
		/*slugCallback: function (filename) {
		  return filename.replace('(', '_').replace(']', '_');
		}*/
	}).on("filebatchselected", function(event, files) {
		//选择文件后处理
		//$(this).fileinput("upload");
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		if(hshshss == "0"){
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin(previewId);
			hshshss = "1";
		}
	}).on('filebatchuploaderror', function(event, data,  previewId, index) {
		if(hshshss == "0"){
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin(previewId);
			hshshss = "1";
		}
    	//$.xalert({title:'提示',msg:'导入成功！'});
	}).on('filepreupload', function(event, data, previewId, index) {    
	}).on('fileerror', function(event, data, msg) {
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	});
}
//关闭从excel导入测试用例弹窗
function closeUploadFromExcelWin(){
	$("#uploadForm input").val("");
	$("#uploadFromExcelWin").xwindow('close');
}

function postCloseXmindWin(data){
	$("#xmind2ExcelWin").xwindow('close');
	if((data.split("<pre>")[1].split("</pre>")[0]).indexOf("success")<0 ){
		$.xalert({title:'提示',msg:data.split("<pre>")[1].split("</pre>")[0]});
	}else{
		var fileName = data.split("<pre>")[1].split("</pre>")[0].split("_")[1];
		var url = baseUrl + '/impExpMgr/caseImpExpAction!downLoadXmind2ExcelFile.action?dto.impFilePath='+fileName;
		xmindurl = url;
		document.getElementById("dwExcelFile").click();
		
		//$("#dwExcelFile").prop('href',url);
		//exeDwExcel($("#dwExcelFile"),url);
		
	}
} 

function exeDwExcel(obj){
	//alert(url);
	$(obj).prop('href',xmindurl);
}
function showXmind2ExcelWin(){
	hshshss = "0";
	//alert(fileType=="xmind");
	$("#xmind2ExceUploadForm input").val("");
	$("#xmind2ExcelWin").xwindow('setTitle','转换').xwindow('open');
	$("#2excelXmindFile").fileinput("destroy");
	$("#2excelXmindFile").fileinput({
		theme: 'fa',
		language:'zh',
		uploadUrl:baseUrl + '/impExpMgr/caseImpAction!convert2Excel.action?dto.pckId='+pckId, // you must set a valid URL here else you will get an error  ['xlsx', 'xls']
		//uploadUrl:baseUrl + '/impExpMgr/caseImpAction!impCase.action', // you must set a valid URL here else you will get an error
		allowedFileExtensions: ['xlsx', 'xls', 'xmind'],
		showPreview:false,
		showClose:false,
		overwriteInitial: true,
		uploadAsync:false,
		autoReplace: true,
		showUploadedThumbs:false,
		maxFileSize: 6000,
		maxFileCount:1,
		imageMaxWidth : 200,
		imageMaxHeight : 100,
		enctype: 'multipart/form-data',
		//msgFilesTooMany :"选择图片超过了最大数量", 
		showRemove:false,
		showClose:false,
		showUpload:false,
		showDownload: false,
		allowedPreviewTypes: ['xlsx', 'xls', 'xmind'],
		dropZoneEnabled:false,
		initialPreviewAsData: false,
		/*slugCallback: function (filename) {
		  return filename.replace('(', '_').replace(']', '_');
		}*/
	}).on("filebatchselected", function(event, files) {
		//选择文件后处理
		//$(this).fileinput("upload");
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		if(hshshss == "0"){
			postCloseXmindWin(previewId);
			hshshss = "1";
		}
	}).on('filebatchuploaderror', function(event, data,  previewId, index) {
		if(hshshss == "0"){
			
			postCloseXmindWin(previewId);
			hshshss = "1";
		}
    	//$.xalert({title:'提示',msg:'导入成功！'});
	}).on('filepreupload', function(event, data, previewId, index) {    
	}).on('fileerror', function(event, data, msg) {
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
		/*$("#uploadFromExcelWin").xwindow('close');
		closeUploadWin();
		caseTree();
    	$.xalert({title:'提示',msg:'导入成功！'});*/
	});
	
}

function uploadXmind2Excel(){
	if(!$(".file-caption-name").val()){
		$.xalert({title:'提示',msg:'请选择需要转换的xmind思维导图文件！'});
		return;
	}
	$("#2excelXmindFile").fileinput("upload");
	
}
//关闭从xmind2excel win 
function closeXmind2ExcelWin(){
	$("xmind2ExceUploadForm input").val("");
	$("#xmind2ExcelWin").xwindow('close');
}
//选择excel文件事件监听
/*function fileChange(obj) {
	$("#excelField").val($(obj).val());
}*/
//确认导入（Excel）
function sureToUpload(){
	if(!$(".file-caption-name").val()){
		$.xalert({title:'提示',msg:'请选择需要导入的Excel文件！'});
		return;
	}
	$("#importFile").fileinput("upload");
	/*$("#uploadForm").ajaxSubmit({
		type: 'post',
		url: baseUrl + '/impExpMgr/caseImpExpAction!impCase.action',
		data: '#uploadForm',
		dataType: 'json',
		beforeSubmit: function() {
			
		},
		success: function(data) {
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin();
			caseTree();
	    	$.xalert({title:'提示',msg:'导入成功！'});
		},
		error: function () {
			$("#uploadFromExcelWin").xwindow('close');
			closeUploadWin();
			caseTree();
	    	$.xalert({title:'提示',msg:'导入成功！'});
        }
	});*/
}
//下载Excel导入摸板
function downloadExcelModel(obj){
	var href = baseUrl + '/impExpMgr/caseImpExpAction!downLoadCaseTempl.action?dto.pckId='+pckId;
	$(obj).prop('href', href);
}
//@ sourceURL=testCasePackageManager.js