var objs = {
	$menuTreeSwitchBtn: $("#menu_tree_switch_btn"),
	$testTaskDg: $("#testTaskDg"),
	$addOrEditWinSin: $("#addOrEditWinSin"),
	$addOrEditForm: $("#addOrEditSinForm"),
	$userGroup: $("#userGroup"),
	$pm: $("#pm"),
	$uploadForm: $("#uploadForm"),
	$testFlowWin: $("#testFlowWin"),
	$workflowForm: $("#workflowForm"),
	$versionMaint:$("#versionMaintenance"),
	$newCreateVer:$("#newCreateVersion"),
	$selectPmWin:$("#selectPmWin"),
	$pMList:$("#pMList"),
	$editProStatus:$("#editProStatus")
};
//存放所有项目
var projectList = [];
//存放选中的项目
var proJ = "";
//存放被选中的参与者
var selectedPeople = [];
//存放被选中的关注者
var selectedConcerns = [];
// 打开新增弹窗
function showAddWin() {
	if(privilegeMap["singleTestTaskAction!add"]!="1"){
		tipPermission('您目前还没有新建项目的权限！');
		return;
	}
	$("#updateTaskId").data('taskI',"");
	objs.$editProStatus.hide();
	objs.$addOrEditWinSin.xform('clear');
	objs.$addOrEditForm.xform('clear');
	objs.$addOrEditWinSin.parent().css("border","none");
	objs.$addOrEditWinSin.prev().css({ color: "#ffff", background: "#101010" });
	//获取相关人员
	relaUser();
	objs.$addOrEditWinSin.xwindow('setTitle','新建项目').xwindow('open');
	
	//加载所有可执行任务的员工
	loadPeopleLists();
	//加载任务类别列表
	loadMissionCategory();
	//加载任务紧急程度列表
	loadEmergencyDegree();
	//加载任务难易程度列表
	loadDifficultyDegree();
}

//保存并设置流程
function saveAndProjectFlow(){
	submit();
	var saveFlowV = $("#saveFlow").val();
	if(saveFlowV){
		showTestFlowWin(saveFlowV);
	}
}

//打开流程设置弹窗
function showTestFlowWin(task_Id_type) {
	$('a[href="#version"]').parent().removeAttr('class');
	$('a[href="#workflow"]').parent().addClass('active');
	$("#workflow").addClass('tab-pane fade active in');
	$("#version").removeClass("tab-pane fade active in").addClass('tab-pane fade');
	var windowEle = objs.$testFlowWin.prev();
	
//	objs.$testFlowWin.parent().css('width','603px');
	windowEle.parent().css("border","none");
	windowEle.css({ color: "#ffff", background: "#101010" });
	
	var taskVal = task_Id_type.split("_");
	var tasid = taskVal[0];
	var taskType = taskVal[1];

	//获取流程设置人员数据
	getPersonData();
	
	//获取流程设置的数据
	getFlowData(tasid,taskType);
	$("#version").next().hide();
}


//获取流程设置人员数据
function getPersonData(){
	var url = baseUrl +'/userManager/userManagerAction!loadDefaultSelUser.action?dto.getPageNo=1&dto.pageSize=100';
	$.ajax({
		  url: url,
		  cache: false,
		  async: false,
		  type: "POST",
		  dataType:"json",
		  success: function(data){
			if(data!=null){
				//获取人员名单
				getUserInfo(data);
			}
		  }
		});
}


//获取流程设置的数据
function getFlowData(taId,taType){
	var url = baseUrl + "/testTaskManager/testTaskManagerAction!flwSetInit.action?dto.taskId=" + taId +"&dto.taskType="+taType+"&dto.comeFrom=flwSetList";
	$.ajax({
		  url: url,
		  cache: false,
		  async: false,
		  type: "POST",
//		  dataType:"json",
		  dataType:"text",
		  success: function(data){
			  var dataJson ="";
			  if(data=="deny"){
				  $.xalert({title:'提示',msg:'无查看测试流程的权限！'});
				  return;
			  }else{
				 dataJson =  JSON.parse(data);
				 if(dataJson != null){
					 setHiddenData(dataJson);
					 $("#testPersonI").data("dataIn",data);
					 if(dataJson.taskName != ""&& dataJson.taskName != null){
						 $("#taskName").text(dataJson.taskName);
					 }else{
						 $("#taskName").text('暂无');
					 }
					 
					 if(dataJson.testLead != ""&& dataJson.testLead != null){
						 var testLeadId = dataJson.testLeadId.replace(/ /g, ",");
						 $(".testLead").xcombobox("setValues",testLeadId.split(","));
					 }else{
						 $(".testLead").xcombobox("setValues",[]);
					 }
					 
					 var inputNum = $("#workflowForm table:eq(1)").find($("input[class='textbox-value'][name^='dto.']"));
					 var inputNums = $("#workflowForm table:eq(1)").find($("input[type='checkbox'][name^='dto.']"));
					 var inputMess = $("#workflowForm table:eq(2)").find($("input[type='checkbox'][name^='dto.detail']"));
					 $.each(inputNum, function(i, n){
						  var dtoNameS = inputNum[i].name;
						  var writeValue = dtoNameS.split(".")[1];
						  var dtoNameF = "";
						  var textWord = "";
						  if(inputNums[i]!=undefined){
							  dtoNameF = inputNums[i].name;
//							  textWord = $($("input[textboxname='"+dtoNameS+"']").parents()['1']).find($("input[name='"+dtoNameF+"']"));
							  textWord = $("input[textboxname='"+dtoNameS+"']").parent().prev().find('input');
						  }
							 //设置人员与id关联，选中
							 setPersonAndIdSel(writeValue,dataJson);
//						 	 //是否勾上checkbox
						 	 isOrNOCheckBox(textWord,dtoNameF);
						  
						 var lastInput = inputNums[inputNums.length-1].name;
						 var dtoSuffix = lastInput.split(".")[1];
						 var testerVerifyFix = dtoSuffix + "=true";
						 if(dataJson.testFlwKeyValueStr!=null&&i<1){
							 var valueFix = dataJson.testFlwKeyValueStr.split("^");
							 for(var a in valueFix){
								 if(valueFix[a] == testerVerifyFix){
									 $('input[name="'+lastInput+'"]').next().css("color","#101010");
									 var loadposi=$('input[name="'+lastInput+'"][type="checkbox"]');
									 if(loadposi.length!=1){
										 loadposi['1'].checked=true;
									 }else{
										 loadposi['0'].checked=true;
									 }
								 }
							 }
						 }
						 
						 if(i < inputMess.length){
							 var messageSet = inputMess[i].name;
							 var sub1 = messageSet.split(".")[1];
							 var sub2 = messageSet.split(".")[2];
	 						 if(dataJson[sub1][sub2] == 1){
	 							 var messL = $('input[name="'+messageSet+'"][type="checkbox"]');
	 							 if(messL.length!=1){
	 								messL['1'].checked=true;
	 							 }else{
	 								messL['0'].checked=true;
	 							 }
							 	 $('input[name="'+messageSet+'"]').next().css("color","#101010");
							 }
						 }
						 
					});
				 }
				 objs.$testFlowWin.xwindow('setTitle','设置测试流程').xwindow('open');
					$('#testBugPerson').prop("checked","checked");
					$('#fixBugPerson').prop("checked","checked");
					$('#arbPerson').prop("checked","checked");
					$('#testSure').prop("checked","checked");
					
					$("#testFlowWin").parent().css('width','630px');
					$("#workflowForm table[id='secondTable']").find($("input[class='textbox-value'][name^='dto.']")).prev().find('input').css({ width: "337px",height: "34px",borderRadius:"5px",fontWeight:"normal"});
			  }
			  
			 
		  }
		});
}

//获取人员名单
function getUserInfo(datas){
	$(".testLead").xcombobox({
		valueField:'keyObj',
	    textField:'valueObj',
	    prompt:'-请选择-',
		multiple:true,
		editable:false,
		data:datas.rows,
		hasDownArrow:true
	});
}

//获取相关人员
function relaUser(){	
	$.post(
			baseUrl +'/userManager/userManagerAction!loadDefaultSelUser.action?dto.getPageNo=1&dto.pageSize=1000',
			null,
			function(data) {
				if (data != null) {
					var opti = '<option value="">-请选择PM-</option>';
					if(data.rows.length > 0){
						for(var i=0;i<data.rows.length;i++){
							opti = opti + '<option value="'+data.rows[i].keyObj+'">'+data.rows[i].valueObj+'</option>';
						}
					}
					$(".searchable-select").remove();
					$("#pm").html(opti);
					$('#pm').searchableSelect();
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}

function searchPm(){
	var keyId = $("#pm option:selected").val();
	var valueText = $("#pm option:selected").text();
	$("#psmId").val(keyId);
	$("#pmId").val(valueText);
}


//提交保存新增的记录
function submit() {
	var urls ="";
	var taskIds = $("#updateTaskId").data('taskI');
	var addOrSelectVal = "";
	//检测必填数据
	var flag = requiredTestData();
	if(flag!=true){
		return;
	}
	
	
	addOrSelectVal = $('input[name="dto.singleTest.proName"][value!=""]').prev().val();

	
	if(taskIds!=""&&taskIds!=undefined){
		urls = baseUrl + "/singleTestTask/singleTestTaskAction!update.action";
		//更新
		$.post(
				urls,
				{
						"dto.singleTest.taskId":taskIds,
						"dto.singleTest.filterFlag":$.trim($(".filterFlag").val()),
						"dto.singleTest.proName":addOrSelectVal,
						"dto.singleTest.psmId":$("#psmId").val(),
						"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
						"dto.singleTest.taskProjectId":$.trim($(".taskProjectId").val()),
						"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
						"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
						"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
						"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
						/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
				},
			function(data) {
				if (data !=null) {
					objs.$addOrEditWinSin.xform('clear');
					objs.$addOrEditForm.xform('clear');
					objs.$addOrEditWinSin.xwindow('close');
//					$("#addOrEditWinSin input").val("");
//					loadTestProject();
					objs.$testTaskDg.xdatagrid('reload');
//					saveAndProjectFlow(data.taskId,data.taskType);
					$("#saveFlow").val(data.taskId+"_"+data.taskType);
				} else {
//					$.xnotify("系统错误！", {type:'warning'});
//					tip("系统错误！");
//					if(data =="existed"){
						objs.$addOrEditForm.xform('clear');
						objs.$addOrEditWinSin.xwindow('close');
						$.xalert({title:'提示',msg:'该项目名称已存在!'});
						$("#saveFlow").val("");
//					}
				}
			}, "json");
	}else{
		urls = baseUrl + "/singleTestTask/singleTestTaskAction!add.action";
		//保存
		//先保存project表
		$.post(baseUrl + "/otherMission/otherMissionAction!addProject.action",{
			"dto.project.projectType":"1",
			"dto.project.createId":$("#accountId").text(),
			"dto.project.projectName":addOrSelectVal
		},function(data){
			if(data.oprateResult == "success"){
				//保存singletask表
				$.post(
						urls,
						{
								"dto.singleTest.taskId":"",
								"dto.singleTest.filterFlag":"1",
								"dto.singleTest.proName":addOrSelectVal,
								"dto.singleTest.psmId":$("#psmId").val(),
								"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
								"dto.singleTest.taskProjectId":data.project.projectId,
								"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
								"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
								"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
								"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
								/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
						},
					function(da) {
						if (da !=null) {
							objs.$addOrEditWinSin.xform('clear');
							objs.$addOrEditForm.xform('clear');
							objs.$addOrEditWinSin.xwindow('close');
							objs.$testTaskDg.xdatagrid('reload');
							$("#saveFlow").val(da.taskId+"_"+da.taskType);
						} else {
								objs.$addOrEditForm.xform('clear');
								objs.$addOrEditWinSin.xwindow('close');
								$.xalert({title:'提示',msg:'该项目名称已存在!'});
								$("#saveFlow").val("");
						}
					}, "json");
			}else{
				objs.$addOrEditForm.xform('clear');
				objs.$addOrEditWinSin.xwindow('close');
				$.xalert({title:'提示',msg:'该项目名称已存在!'});
				$("#saveFlow").val("");
			}
		}, "json");
	}
}

//检测必填数据
function requiredTestData(){
	if($('.projectNums').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目编号!'});
		return false;
	}
	
	if($('.hadProject').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写项目名称!'});
		return false;
	}
	
	if($('.developmentDep').xtextbox('getValue')==""){
		$.xalert({title:'提示',msg:'请填写研发部门!'});
		return false;
	}
	
	if($('.proStName').val()==""){
		$.xalert({title:'提示',msg:'请选择PM!'});
		return false;
	}
	
	var planStartDate = $('.startTime').xdatebox('getValue');
	var planEndDate = $('.endTime').xdatebox('getValue');
	
	var startTime = planStartDate.replace(/-/g,"/");//替换字符，变成标准格式
	var endTime = planEndDate.replace(/-/g,"/");//替换字符，变成标准格式
	
	var time1 = new Date(Date.parse(startTime));  
	var time2 = new Date(Date.parse(endTime));
	
	if(time1 > time2){
		$.xalert({title:'提示',msg:'开始日期大于结束日期!'});
		return false;
	}
	
	if(planStartDate==""){
		$.xalert({title:'提示',msg:'请选择开始日期!'});
		return false;
	}
	if(planEndDate==""){
		$.xalert({title:'提示',msg:'请选择结束日期!'});
		return false;
	}
	
	return true;
}

//取消提交并关闭弹窗
function closeWin() {
	$("#updateTaskId").data('taskI',"");
	objs.$addOrEditWinSin.xwindow('close');
}

//设置隐藏域的值
function setHiddenData(datas){
	 $("#testFlowStr").val(datas.testFlowStr);
	 $("#initFlowStr").val(datas.initFlowStr);
	 $("#taskType").val(datas.taskType);
	 $("#taskIds").val(datas.detail.taskId);
	 $("#testPhase").val(datas.detail.testPhase);
	 $("#relaTaskId").val(datas.detail.relaTaskId);
	 $("#outlineState").val(datas.detail.outlineState);
	 $("#taskState").val(datas.detail.taskState);
	 $("#customCase").val(datas.detail.customCase);
	 $("#customBug").val(datas.detail.customBug);
	 $("#testerId").val(datas.testerId);
	 $("#testerConfId").val(datas.testerConfId);
	 $("#analyserId").val(datas.analyserId);
	 $("#assignationId").val(datas.assignationId);
	 $("#programmerId").val(datas.programmerId);
	 $("#empolderAffirmantId").val(datas.empolderAffirmantId);
	 $("#empolderLeaderId").val(datas.empolderLeaderId);
	 $("#testLeadId").val(datas.testLeadId);
	 $("#testTaskState").val(datas.detail.testTaskState);
	 $("#projectId").val(datas.detail.projectId);
	 $("#caseReviewId").val(datas.caseReviewId);
	 $("#testSeq").val(datas.detail.testSeq);
	 $("#proRelaPersonId").val(datas.proRelaPersonId);
}

var flg = "";
//新建或者修改版本信息
function newCreateOrEditVer(flag){
	$("#newCreateVersion").parent().css("left","410px");
	$("#newCreateVersion").parent().css("border","none");
	$("#newCreateVersion").prev().css({ color: "#ffff", background: "#101010" });
	if(flag == 'edit'){
		saveOrEditVersion(flag);
	}else{
		objs.$newCreateVer.xwindow('setTitle','新建版本').xwindow('open');
	}
}

//提交修改信息
function submitInfo(){
	var testFlowStr=$("#testFlowStr").val();//修改之后拼接的
	var initFlowStr=$("#initFlowStr").val();//修改之前的返回来的
	
	//校验数据
	var flag = checkWriteData();
	if(typeof flag != "undefined" &&(!flag)==true){
		return;
	}
	
	var check = selectCheckedToInput();
	if(!check){
		return;
	}
	
	$('input[name="dto.reportBug"]').checked=true;
	$('input[name="dto.devFixBug"]').checked=true;
	$('input[name="dto.arbitrateBug"]').checked=true;
	$('input[name="dto.testerVerifyFix"]').checked=true;
	if($('input[name="dto.detail.mailOverdueBug"]').attr('checked')=='checked'&&$('input[name="dto.proRelaPersonFlg"]').attr('checked')==undefined){
		$.xalert({title:'提示',msg:'因没勾选项目关注，通知项目关注人设置无法生效!'});
		return;
	}
	var inputCh = $("#workflowForm table").find($("input[type='checkbox'][name^='dto.']"));
	$.each( inputCh, function(i, n){
		  var inputCheck = inputCh[i].checked;
		  var dtoName =  inputCh[i].name;
		  if(inputCheck == true){
				switch(dtoName)
				{
				case "dto.caseReview":
					$('input[name="'+dtoName+'"]').val('9');
				    break;
				case "dto.reportBug":
					$('input[name="'+dtoName+'"]').val('1');
					break;
				case "dto.testerBugConfirm":
					$('input[name="'+dtoName+'"]').val('2');
					break;
				case "dto.analyse":
					$('input[name="'+dtoName+'"]').val('3');
					break;
				case "dto.assignation":
					$('input[name="'+dtoName+'"]').val('4');
					break;
				case "dto.devFixBug":
					$('input[name="'+dtoName+'"]').val('5');
					break;
				case "dto.devConfirmFix":
					$('input[name="'+dtoName+'"]').val('6');
					break;
				case "dto.arbitrateBug":
					$('input[name="'+dtoName+'"]').val('7');
					break;
				case "dto.proRelaPersonFlg":
					$('input[name="'+dtoName+'"]').val('11');
					break;
				case "dto.testerVerifyFix":
					$('input[name="'+dtoName+'"]').val('8');
					break;
				case "dto.detail.mailOverdueBug":
					$('input[name="'+dtoName+'"]').val('1');
					break;
				case "dto.detail.mailDevFix":
					$('input[name="'+dtoName+'"]').val('1');
					break;
				case "dto.detail.mailVerdict":
					$('input[name="'+dtoName+'"]').val('1');
					break;
				case "dto.detail.mailReport":
					$('input[name="'+dtoName+'"]').val('1');
					break;
				default:
					break;
				}
		  }else{
			  $('input[name="'+dtoName+'"]').val('');
		  }
	});
	
	var testFlowVal = $("#workflowForm table").find($("input[type='checkbox'][name^='dto.']")).not($("input[type='checkbox'][name*='dto.detail']"));
	var realV =[];
	$.each(testFlowVal,function(i,n){
		var tesVal = testFlowVal[i].value;
		if(tesVal!=""){
			realV.push(parseInt(tesVal));
		}
	});
	
	realV=realV.sort(compare);
	
	$("#testFlowStr").val("");
	$("#testFlowStr").val(realV.toString());
	var dtoTestLead = $('.test_Lead').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoTester = $('.testerId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoTesterConf = $('.testerConfId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoAnalyser = $('.analyserId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoAssigner = $('.assignationId').xcombobox("getValues").toString().replace(/,/g, " ");
	
	var dtoProgrammer = $('.programmerId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoEmpolderAffirmant = $('.empolderAffirmantId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoEmpolderLeader = $('.empolderLeaderId').xcombobox("getValues").toString().replace(/,/g, " ");
	var dtoProRelaPerson = $('.proRelaPersonId').xcombobox("getValues").toString().replace(/,/g, " ");
	
	//下拉选择后，把页面隐藏域的值跟新
	$("#testLeadId").val(dtoTestLead);
	$("#testerId").val(dtoTester);
	$("#testerConfId").val(dtoTesterConf);
	$("#analyserId").val(dtoAnalyser);
	$("#assignationId").val(dtoAssigner);
	$("#programmerId").val(dtoProgrammer);
	$("#empolderAffirmantId").val(dtoEmpolderAffirmant);
	$("#empolderLeaderId").val(dtoEmpolderLeader);
	$("#proRelaPersonId").val(dtoProRelaPerson);
	
	var taskType="";
	if(taskType==""){
		taskType = "0";
	}else{
		taskType = $("#taskType").val();
	}
	
	var dataMap = {
			"dto.testFlowStr":realV.toString(),
			"dto.initFlowStr":initFlowStr,
			"dto.detail.taskId":$("#taskIds").val(),
			"dto.taskId":$("#taskIds").val(),
			"dto.taskType":taskType,
			
			"dto.testerId":$("#testerId").val(),
	        "dto.testerConfId":$("#testerConfId").val(),
		    "dto.analyserId":$("#analyserId").val(),
		    "dto.assignationId":$("#assignationId").val(),
		    "dto.programmerId":$("#programmerId").val(),
		    "dto.empolderAffirmantId":$("#empolderAffirmantId").val(),
			"dto.empolderLeaderId":$("#empolderLeaderId").val(),
		    "dto.testLeadId":$("#testLeadId").val(),
		    "dto.caseReviewId":$("#caseReviewId").val(),
		    "dto.proRelaPersonId":$("#proRelaPersonId").val(),
		    "dto.detail.outlineState":$("#outlineState").val(),
		    "dto.detail.testTaskState":$("#testTaskState").val(),
		    
		    "dto.testLead":dtoTestLead,
//		    "dto.caseReviewer":dtoCaseReviewer,
		    "dto.tester":dtoTester,
		    "dto.testerConf":dtoTesterConf,
		    "dto.analyser":dtoAnalyser,
		    "dto.assigner":dtoAssigner,
		    "dto.programmer":dtoProgrammer,
		    "dto.empolderAffirmant":dtoEmpolderAffirmant,
		    "dto.empolderLeader":dtoEmpolderLeader,
		    "dto.proRelaPerson":dtoProRelaPerson,
		    
		    "dto.reportBug":$('input[name="dto.reportBug"]').val(),
		    "dto.testerBugConfirm":$('input[name="dto.testerBugConfirm"]').val(),
		    "dto.analyse":$('input[name="dto.analyse"]').val(),//on
		    "dto.assignation":$('input[name="dto.assignation"]').val(),//on
		    "dto.devFixBug":$('input[name="dto.devFixBug"]').val(),
		    "dto.devConfirmFix":$('input[name="dto.devConfirmFix"]').val(),
		    "dto.arbitrateBug":$('input[name="dto.arbitrateBug"]').val(),
		    "dto.proRelaPersonFlg":$('input[name="dto.proRelaPersonFlg"]').val(),
		    "dto.testerVerifyFix":$('input[name="dto.testerVerifyFix"]').val(),
		    "dto.detail.mailOverdueBug":$('input[name="dto.detail.mailOverdueBug"]').val(),
		    "dto.detail.mailDevFix":$('input[name="dto.detail.mailDevFix"]').val(),
		    "dto.detail.mailVerdict":$('input[name="dto.detail.mailVerdict"]').val(),
		    "dto.detail.mailReport":$('input[name="dto.detail.mailReport"]').val()
	};
	
	var url=baseUrl+"/testTaskManager/testTaskManagerAction!update.action";
	$.ajax({
		  url: url,
		  cache: false,
		  async: false,
		  type: "POST",
		  dataType:"text",
		  data: dataMap,
		  success: function(data){
			  if(data == "success"){
				  objs.$testFlowWin.xwindow('close');
				  objs.$testFlowWin.xform('clear');
					$.xalert({title:'提示',msg:'操作成功!'});

			  }else{

				  $.xalert({title:'提示',msg:'系统问题!'});
			  }
		  }
		});
}

//重置测试流程信息
function resetting(){
	$(".testPerson").css("color","#353535");
	objs.$workflowForm.xform('clear');
	$('input[type="checkbox"][name^="dto."]').removeAttr('checked');
	$('input[class="textbox-f"][textboxname^="dto."]').prev().css("color","#353535");
	$('input[type="checkbox"][name^="dto."]').next().css("color","#636363");

}

//关闭弹窗
function returnWin(){
	objs.$testFlowWin.xwindow('close');
	objs.$testFlowWin.xform('clear');
}

//校验数据
function checkWriteData(){
	
	if($('.test_Lead').xcombobox("getValues").length==0){
		$.xalert({title:'提示',msg:'请选择测试负责人!'});
		return false;
	}else if($('.testerId').xcombobox("getValues").length==0){
		$.xalert({title:'提示',msg:'请选择测试人员!'});
		return false;
	}else if($('.programmerId').xcombobox("getValues").length==0){
		$.xalert({title:'提示',msg:'请选择修改人!'});
		return false;
	}else if($('.empolderLeaderId').xcombobox("getValues").length==0){
		$.xalert({title:'提示',msg:'请选择仲载人!'});
		return false;
	}
	return true;
}

//校验如果勾选了左边的checkbox，右边的input就必须有值
function selectCheckedToInput(){
	var SelInputCh = $("#workflowForm table").find($("input[type='checkbox'][name^='dto.']")).not($("input[type='checkbox'][name*='dto.detail']"));
	var flagTip=true;
	var dtoNames ="";
	for(var i=0;i<SelInputCh.length;i++){
		  var inputChecks = SelInputCh[i].checked;
		  dtoNames =  SelInputCh[i].name;
		  if(inputChecks){
			    if(dtoNames=="dto.reportBug"){
			    	flagTip=submitTip("dto.reportBug","testerId");
			    }else if(dtoNames=="dto.testerBugConfirm"){
			    	flagTip=submitTip("dto.testerBugConfirm","testerConfId");
			    }else if(dtoNames=="dto.analyse"){
			    	flagTip=submitTip("dto.analyse","analyserId");
			    }else if(dtoNames=="dto.assignation"){
			    	flagTip=submitTip("dto.assignation","assignationId");
			    }else if(dtoNames=="dto.devFixBug"){
			    	flagTip=submitTip("dto.devFixBug","programmerId");
			    }else if(dtoNames=="dto.devConfirmFix"){
			    	flagTip=submitTip("dto.devConfirmFix","empolderAffirmantId");
			    }else if(dtoNames=="dto.arbitrateBug"){
			    	flagTip=submitTip("dto.arbitrateBug","empolderLeaderId");
			    }else if(dtoNames=="dto.proRelaPersonFlg"){
			    	flagTip=submitTip("dto.proRelaPersonFlg","proRelaPersonId");
			    }else{
			    	flagTip=submitTip("dto.testerVerifyFix","");
			    }
			    if(!flagTip){
			    	flagTip = false;
			    	break;
			    }
		  }
	}
	return flagTip;
}

function submitTip(paramDto,className){
	if(className!=""){
		var classVal = $("."+className).xcombobox("getValues")[0];
		var classNameLen = $("."+className).xcombobox("getValues").length;
		switch(paramDto)
		{
		case "dto.reportBug":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择测试人员！'});
				return false;
			}else{
				return true;
			}
			
			break;
		case "dto.testerBugConfirm":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择互验人员！'});
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.analyse":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择分析人！'});
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.assignation":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择分配人！'});
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.devFixBug":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择修改人！'});
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.devConfirmFix":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择互检人！'});
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.arbitrateBug":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择仲裁人！'});	
				 return false;
			}else{
				return true;
			}
			break;
		case "dto.proRelaPersonFlg":
			if(classNameLen==0){
				$.xalert({title:'提示',msg:'请选择关注人！'});
				 return false;
			}else{
				return true;
			}
			break;
		default:
			break;
		}
	}else{
		return true;
	}

}

function compare(x, y) {//比较函数
    if (x < y) {
        return -1;
    } else if (x > y) {
        return 1;
    } else {
        return 0;
    }
}


//保存版本信息
function saveOrEditVersion(fl){
	if(fl == "edit"){
		var row = objs.$versionMaint.xdatagrid('getSelected');
		if (!row) {
//			$.xnotify('请选择要修改的一条记录', {type:'warning'});
//			tip("请选择要修改的一条记录!");
			$.xalert({title:'提示',msg:'请选择要修改的一条记录!'});
			return;
		}
		updateVerInfo(row);//修改版本信息
	}else{
		//检验必填数据
		var testFlag = requireTestInfo();
		if(testFlag!=true){
			return;
		}
		saveverInfo();//保存版本信息
	}
}

//取消保存版本信息
function cancleVers(){
	objs.$newCreateVer.xform('clear');
	objs.$newCreateVer.xwindow('close');
}

//删除版本信息
function deleteVer(){
	var row = objs.$versionMaint.xdatagrid('getSelected');
	if (!row) {
//		$.xnotify('请选择要修改的一条记录', {type:'warning'});
//		tip("请选择要删除的一条记录!");
		$.xalert({title:'提示',msg:'请选择要删除的一条记录!'});
		return;
	}
	
	$.xconfirm({
		msg:'您确定删除选择的记录吗?',
		okFn: function() {
			$.post(
				baseUrl+"/testTaskManager/testTaskManagerAction!delSoftVer.action",
				{"dto.softVer.versionId":row.versionId,"dto.softVer.taskid":row.taskid},
				function(data) {
					if (data.indexOf("success") >= 0) {
						 loadVersion();
					} else {
//						$.xnotify(data, {type:'warning'});
//						tip(data);
						$.xalert({title:'提示',msg:data});
					}
				}, "text");
		}
		
	});
}

//启用版本信息
function startVersion(){
	var row = objs.$versionMaint.xdatagrid('getSelected');
	if (!row) {
//		$.xnotify('请选择要修改的一条记录', {type:'warning'});
//		tip("请选择要启用的记录!");
		$.xalert({title:'提示',msg:'请选择要启用的记录!'});
		return;
	}
	
	$.xconfirm({
		msg:'您确定启用选择的记录吗?',
		okFn: function() {
			$.post(
				baseUrl+"/testTaskManager/testTaskManagerAction!softVerSwStatus.action",
				{"dto.softVer.versionId":row.versionId,"dto.softVer.taskid":row.taskid,"dto.softVer.verStatus":"0"},
				function(data) {
					if (data.indexOf("success") >= 0) {
						 loadVersion();
					} else {
//						$.xnotify(data, {type:'warning'});
//						tip(data);
						$.xalert({title:'提示',msg:data});
					}
				}, "text");
		}
		
	});
}

//加载版本维护信息
function loadVersion(){
	var taskid = $('#taskIds').val();
	var urls = baseUrl + '/testTaskManager/testTaskManagerAction!softVerListLoad.action?dto.taskId='+taskid;
	objs.$versionMaint.xdatagrid({
		url: urls,
		method: 'get',
		height: mainObjs.tableHeight,
		emptyMsg:"暂无数据",
	    columns:[[
	        {field:'taskid',hidden:true},
			{field:'seq',title:'序号',width:'100',align:'center'},
			{field:'versionNum',title:'版本名称',width:'90',align:'left',halign:'center'},
			{field:'versionId',title:'顺序号',width:'110',align:'center',halign:'center'},
			{field:'remark',title:'备注',width:'100',align:'left',halign:'center'},
			{field:'verStatus',title:'状态',width:'120',align:'center',halign:'center',formatter:softverStatus}
	    ]],
	    onClickRow:function(rowIndex, rowData){
	    	var verStatusR = rowData.verStatus;
	    	if(verStatusR == "0"){
	    		$('button[onclick="stopVersion()"]').css('display','');
	    		$('button[onclick="startVersion()"]').css('display','none');
	    	}else{
	    		$('button[onclick="stopVersion()"]').css('display','none');
	    		$('button[onclick="startVersion()"]').css('display','');
	    	}
	    },
	    onLoadSuccess : function (data) {
//	    	$("#versionMaintenance").xdatagrid('resize');
	    	/*if (data.total==0) {
	    		$("#versionMaintenance").prev().find('tr[id^="datagrid-row-r"]').empty();
	    		$("#versionMaintenance").prev().find('tr[id^="datagrid-row-r"]').append('<label style="height: 40px;width: 450px;margin-top: 10px;text-align: center;">暂无数据!</label>');
//				$("#datagrid-row-r2-2-0").parent().append('<label style="height: 40px;width: 450px; text-align: center;">暂无版本数据!</label>');
			}*/
		}
	});
}

function softverStatus(value,row,index){
	var startOrStop ="";
	var verStatus = row.verStatus;
	if(verStatus == "1"){
		startOrStop = "停用";
	}else if(verStatus == "0"){
		startOrStop = "启用";
	}
	return startOrStop;
}

function saveverInfo(){
	var urls = baseUrl+"/testTaskManager/testTaskManagerAction!addSoftVer.action";
	$.ajax({
		  url: urls,
		  cache: false,
		  async: false,
		  type: "POST",
//		  dataType:"json",
		  dataType:"text",
		  data: objs.$newCreateVer.xserialize(),
		  success: function(data){
			  if(data.indexOf("success^") >= 0){
				  cancleVers();
				  loadVersion();
			  }else if(data.indexOf("seqRep") >= 0){
//				  tip("重复版本信息！");
				  $.xalert({title:'提示',msg:'重复版本信息!'});
			  }else{
//				  tip("系统问题！");
				  $.xalert({title:'提示',msg:'系统问题!'});
			  }
		  }
		});
}

//检验必填数据
function requireTestInfo(){
//	var data = objs.$newCreateVer.xdeserialize();
	if($('input[name="dto.softVer.versionNum"]').prev().val()==""){
//		tip("请填写名称！");
		$.xalert({title:'提示',msg:'请填写名称!'});
		return false;
	}
	
	var validNumber = $('.xNumber').xtextbox("getValue");
	if(validNumber==""){
		$.xalert({title:'提示',msg:'请填写序号!'});
		return false;
	}else{
		var re = /^[0-9]+.?[0-9]*$/;
		if (!re.test(validNumber)) {
			$.xalert({title:'提示',msg:'序号是必须为数字!'});
			return false;
		}
	}
	return true;
}

$('a[href="#version"]').click(function(){
	$("div#workflow").attr('class','tab-pane fade');
	$("div#version").attr('class','tab-pane fade in active');
	loadVersion();
});

$('a[href="#workflow"]').click(function(){
	$("#version").next().hide();
//	$("#version")["0"].attributes[1].nodeValue = "tab-pane fade";
//	$($("#testFlowWin").find("div#version")[0]).attr('class','tab-pane fade');
});

//停用版本信息
function stopVersion(){
	var row = objs.$versionMaint.xdatagrid('getSelected');
	if (!row) {
//		$.xnotify('请选择要修改的一条记录', {type:'warning'});
//		tip("请选择要停用的记录!");
		$.xalert({title:'提示',msg:'请选择要停用的记录!'});
		return;
	}
	
	$.xconfirm({
		msg:'您确定停用选择的记录吗?',
		okFn: function() {
			$.post(
				baseUrl+"/testTaskManager/testTaskManagerAction!softVerSwStatus.action",
				{"dto.softVer.versionId":row.versionId,"dto.softVer.taskid":row.taskid,"dto.softVer.verStatus":"1"},
				function(data) {
					if (data.indexOf("success") >= 0) {
						 loadVersion();
					} else {
//						$.xnotify(data, {type:'warning'});
//						tip(data);
						$.xalert({title:'提示',msg:data});
					}
				}, "text");
		}
		
	});
}

function updateVerInfo(rows){
	var url = baseUrl+"/testTaskManager/testTaskManagerAction!udateSoftVer.action";
	$.ajax({
		  url: url,
		  cache: false,
		  async: false,
		  type: "POST",
		  dataType:"json",
//		  dataType:"text",
		  data: {"dto.softVer.versionId":rows.versionId,"dto.softVer.versionNum":rows.versionNum},
		  success: function(data){
			  if(data!=null){
				  objs.$newCreateVer.xdeserialize(data);
				  $("#taId").val(data["dto.softVer"].taskid);
				  objs.$newCreateVer.xwindow('setTitle','修改版本').xwindow('open');
			  }else{
//				  $.xnotify('系统问题！', {type:'warning'});
//				  tip("系统问题！");
				  $.xalert({title:'提示',msg:'系统问题!'});
			  }
		  }
		});
}
//设置人员与id关联，选中
function setPersonAndIdSel(val,data){
	 switch(val)
		{
		case "caseReviewer":
			selPerson('caseReviewId',data,val);
		    break;
		case "analyser":
			selPerson('analyserId',data,val);
			break;
		case "assigner":
			selPerson('assignationId',data,val);
			break;
		case "empolderAffirmant":
			selPerson('empolderAffirmantId',data,val);
			break;
		case "empolderLeader":
			selPerson('empolderLeaderId',data,val);
			break;
		case "proRelaPerson":
			selPerson('proRelaPersonId',data,val);
			break;
		case "programmer":
			selPerson('programmerId',data,val);
			break;
		case "tester":
			selPerson('testerId',data,val);
			break;
		case "testerConf":
			selPerson('testerConfId',data,val);
			break;
		default:
			break;
		}
}
//$("input[name='"+dtoNameS+"']").prev().find("input").val(data[writeValue]);
//在获取人员id，追加数据，回填的人员与下拉人员选中
function selPerson(pId,dat,dtoV){
//	 var content ="";
//	 if(dat[pId]!=""){
//		 var wId = dat[pId].split(" ");
//		 for(var i=0;i<wId.length;i++){
//			 content += '<input type="hidden" class="textbox-value" name="" value="'+wId[i]+'">';
//		 }
//	 }
//	 $('input[name="dto.'+dtoV+'"]').prev().find("input").after(content);
	if(dat[pId]){
		var wId = dat[pId].replace(/ /g, ",");
		$("."+pId).xcombobox("setValues",wId.split(","));
	}else{
		$("."+pId).xcombobox("setValues",[]);
//		$("."+pId).css('width','337px');
	}
}
//是否勾上checkbox
function isOrNOCheckBox(paramdt,dtos){
	if(dtos=="dto.reportBug"){
		//比对值，是否勾中
		checkProperties('1',paramdt,dtos);
    }else if(dtos=="dto.testerBugConfirm"){
    	checkProperties('2',paramdt,dtos);
    }else if(dtos=="dto.analyse"){
    	checkProperties('3',paramdt,dtos);
    }else if(dtos=="dto.assignation"){
    	checkProperties('4',paramdt,dtos);
    }else if(dtos=="dto.devFixBug"){
    	checkProperties('5',paramdt,dtos);
    }else if(dtos=="dto.devConfirmFix"){
    	checkProperties('6',paramdt,dtos);
    }else if(dtos=="dto.arbitrateBug"){
    	checkProperties('7',paramdt,dtos);
    }else{
    	checkProperties('11',paramdt,dtos);
    }
	
 }

////比对值，是否勾中
function checkProperties(num,params,dtoN){
	var testFlow = $("#testFlowStr").val();
	var flowSplit=testFlow.split(",");
		if(flowSplit.indexOf(num)>=0){
			$("input[name='"+dtoN+"'][type='checkbox']").prop("checked","checked");
			$("input[name='"+dtoN+"'][type='checkbox']").val(num);
		}else{
			$("input[name='"+dtoN+"'][type='checkbox']").prop("checked","");
			$("input[name='"+dtoN+"'][type='checkbox']").val("");
		}
}
//保存并创建任务
function saveAndCreateMission(){
	var urls ="";
	var taskIds = $("#updateTaskId").data('taskI');
	var addOrSelectVal = "";
	//检测必填数据
	var flag = requiredTestData();
	if(flag!=true){
		return;
	}
	addOrSelectVal = $('input[name="dto.singleTest.proName"][value!=""]').prev().val();
	if(taskIds!=""&&taskIds!=undefined){
		urls = baseUrl + "/singleTestTask/singleTestTaskAction!update.action";
		//更新
		$.post(
				urls,
				{
						"dto.singleTest.taskId":taskIds,
						"dto.singleTest.filterFlag":$.trim($(".filterFlag").val()),
						"dto.singleTest.proName":addOrSelectVal,
						"dto.singleTest.psmId":$("#psmId").val(),
						"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
						"dto.singleTest.taskProjectId":$.trim($(".taskProjectId").val()),
						"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
						"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
						"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
						"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
						/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
				},
			function(data) {
				if (data !=null) {
					objs.$addOrEditWinSin.xform('clear');
					objs.$addOrEditForm.xform('clear');
					objs.$addOrEditWinSin.xwindow('close');
//					$("#addOrEditWinSin input").val("");
//					loadTestProject();
					objs.$testTaskDg.xdatagrid('reload');
//					saveAndProjectFlow(data.taskId,data.taskType);
					$("#saveFlow").val(data.taskId+"_"+data.taskType);
					proJ = data.taskId;
					showAddWin1();
				} else {
//					$.xnotify("系统错误！", {type:'warning'});
//					tip("系统错误！");
//					if(data =="existed"){
						objs.$addOrEditForm.xform('clear');
						objs.$addOrEditWinSin.xwindow('close');
						$.xalert({title:'提示',msg:'该项目名称已存在!'});
						$("#saveFlow").val("");
//					}
				}
			}, "json");
	}else{
		urls = baseUrl + "/singleTestTask/singleTestTaskAction!add.action";
		//保存
		//先保存project表
		$.post(baseUrl + "/otherMission/otherMissionAction!addProject.action",{
			"dto.project.projectType":"1",
			"dto.project.createId":$("#accountId").text(),
			"dto.project.projectName":addOrSelectVal
		},function(data){
			if(data.oprateResult == "success"){
				//保存singletask表
				$.post(
						urls,
						{
								"dto.singleTest.taskId":"",
								"dto.singleTest.filterFlag":"1",
								"dto.singleTest.proName":addOrSelectVal,
								"dto.singleTest.psmId":$("#psmId").val(),
								"dto.singleTest.status":$(".editSta").xcombobox('getValue'),
								"dto.singleTest.taskProjectId":data.project.projectId,
								"dto.singleTest.proNum":$('input[name="dto.singleTest.proNum"]').prev().val(),
								"dto.singleTest.devDept":$('input[name="dto.singleTest.devDept"]').prev().val(),
								"dto.singleTest.planEndDate":$('input[name="dto.singleTest.planEndDate"]').prev().val(),
								"dto.singleTest.planStartDate":$('input[name="dto.singleTest.planStartDate"]').prev().val()
								/*"dto.singleTest.psmName":$('input[name="dto.singleTest.psmName"]').prev().find($('input[id^="_exui_textbox_input"]')).val(),*/
						},
					function(da) {
						if (da !=null) {
							objs.$addOrEditWinSin.xform('clear');
							objs.$addOrEditForm.xform('clear');
							objs.$addOrEditWinSin.xwindow('close');
//							$("#addOrEditWinSin input").val("");
//							loadTestProject();
							objs.$testTaskDg.xdatagrid('reload');
//							saveAndProjectFlow(data.taskId,data.taskType);
							$("#saveFlow").val(da.taskId+"_"+da.taskType);
							proJ = da.taskId;
							showAddWin1();
						} else {
//							$.xnotify("系统错误！", {type:'warning'});
//							tip("系统错误！");
//							if(data =="existed"){
								objs.$addOrEditForm.xform('clear');
								objs.$addOrEditWinSin.xwindow('close');
								$.xalert({title:'提示',msg:'该项目名称已存在!'});
								$("#saveFlow").val("");
//							}
						}
					}, "json");
			}else{
				objs.$addOrEditForm.xform('clear');
				objs.$addOrEditWinSin.xwindow('close');
				$.xalert({title:'提示',msg:'该项目名称已存在!'});
				$("#saveFlow").val("");
			}
		}, "json");
	}
}
//打开新增弹窗
function showAddWin1() {
	$(".inchargePeople").xcombobox({
		data:[]
	});
	$("#addOrEditForm1").xform('clear');
	$("#addOrEditForm1 input").val("");
	$("#addOrEditWin1").parent().css("border","none");
	$("#addOrEditWin1").prev().css({ color: "#ffff", background: "#101010" });
	$(".missionNa").next().children("input").css({"width":"475px"});
	$(".missionDe").next().children("input").css({"width":"475px","height":"74px"});
	$("#addOrEditWin1").xwindow('setTitle','创建任务').xwindow('open');
	loadProjectsAddMission();
}
//加载新增任务时项目下拉菜单
function loadProjectsAddMission(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getProjectLists.action",
			null,
			function(dat) {
				if (dat != null) {
					projectList = dat.rows;
					//加载下拉菜单选项(为管理员时)
					if($("#isAdmin").text() == "2" || $("#isAdmin").text() == "1"){
						var opti = '<option value="">-请选择项目-</option>';
						if(dat.rows.length > 0){
							for(var i=0;i<dat.rows.length;i++){
								opti = opti + '<option value="'+dat.rows[i].projectId+'">'+dat.rows[i].projectName+'</option>';
							}
						}
						$(".projectIds").next("div.searchable-select").remove();
						$(".projectIds").html(opti);
						$(".projectIds").val(proJ);
						$('.projectIds').searchableSelect();
					}else{
						//加载下拉菜单选项(非管理员时)
						var opti = '<option value="">-请选择项目-</option>';
						$.post(baseUrl + "/otherMission/otherMissionAction!getProjectListsRelated.action?dto.related=create",null,function(datt) {
							if(datt.rows.length > 0){
								for(var p=0;p<datt.rows.length;p++){
									opti = opti + '<option value="'+datt.rows[p].projectId+'">'+datt.rows[p].projectName+'</option>';
								}
							}
							$(".projectIds").next("div.searchable-select").remove();
							$(".projectIds").html(opti);
							$(".projectIds").val(proJ);
							$('.projectIds').searchableSelect();
						},'json');
					}
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//取消提交并关闭弹窗
function closeWin1() {
	$("#addOrEditWin1").xwindow('close');
}
//提交保存新增或修改的记录
function submit1() {
	//获取表单数据
	var objData = $("#addOrEditWin1").xserialize();
	//新增时，将状态设为0
	if(!objData["dto.otherMission.status"]){
		objData["dto.otherMission.status"] = "0";
	}
	//确定projectType，传到后台
	if(proJ){
		for(var i=0;i<projectList.length;i++){
			if(proJ == projectList[i].projectId){
				objData["dto.otherMission.projectType"] = projectList[i].projectType;
				break;
			}
		}
	}
	var saveOrUpdateUrl = "";
	if(objData["dto.otherMission.missionId"]){
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!update.action";
	}else{
		saveOrUpdateUrl = baseUrl + "/otherMission/otherMissionAction!add.action";
		objData["dto.otherMission.createUserId"] = $("#loginName").text();
	}
	
	//将选中的项目执行人拼接到表单数据，传到后台
	if(selectedPeople.length > 0){
		objData["dto.userIds"] = selectedPeople.toString();
	}
	//将选中的任务关注者拼接到表单数据，传到后台
	if(selectedConcerns.length > 0){
		objData["dto.concernIds"] = selectedConcerns.toString();
	}
	
//	for(var i=0;i<selectedPeople.length;i++){
//		objData["dto.userOtherMissions["+i+"].userId"] = selectedPeople[i];
//	}
	if(!objData["dto.otherMission.missionName"] || !objData["dto.otherMission.description"] || !objData["dto.otherMission.chargePersonId"] || selectedPeople.length == 0){
		$.xalert({title:'提交失败',msg:'请填写完整所有必填信息！'});
		return;
	}else{
		//判断是否勾选了同时创建测试项目
		if($('.checkPro').is(':checked')){
			if(!objData["dto.otherMission.predictStartTime"] || !objData["dto.otherMission.predictEndTime"] || !objData["dto.otherMission.projectId"]){
				$.xalert({title:'提交失败',msg:'勾选同时创建测试项目，需选择所属项目，预计开始时间，预计结束时间！'});
				return;
			}
		}
	}
	var t1 = /^\d+$/;
	if(objData["dto.otherMission.standardWorkload"] && !t1.test(objData["dto.otherMission.standardWorkload"])){
		$.xalert({title:'提交失败',msg:'请正确填写完整所有必填项！'});
		return;
	}
	//判断预计开始时间和预计结束时间前后关系
	if(objData["dto.otherMission.predictStartTime"] && objData["dto.otherMission.predictEndTime"]){
		if(objData["dto.otherMission.predictStartTime"] > objData["dto.otherMission.predictEndTime"]){
			$.xalert({title:'提交失败',msg:'预计结束时间必须在预计开始时间之后！'});
			return;
		}
	}
	$.post(
		saveOrUpdateUrl,
		objData,
		function(data) {
			if (data =="success") {
				//判断是否勾选了同时创建测试项目
				if($('.checkPro').is(':checked')){
					
				}else{
					$("#addOrEditWin1").xform('clear');
					$("#addOrEditWin1").xwindow('close');
					$.xalert({title:'提示',msg:'操作成功！'});
				}
			} else if(data =="existed"){
				$.xalert({title:'提交失败',msg:'该任务名称已存在，请勿重复添加！'});
			}else {
				$.xalert({title:'提交失败',msg:'系统错误！'});
			}
		}, "text");
}

//选择项目改变事件
function changeProject(obj){
	proJ = obj.value;
}
//加载可执行任务员工列表
function loadPeopleLists(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					peopleList = dat.rows;
					$(".peopleList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedPeople = newValue;
							var chargePeopleList = [];
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleList.push(dat.rows[j]);
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".peopleList").next().children().next().attr("title",chargePeopleNameList.toString());
								$(".inchargePeople").xcombobox({
									data:chargePeopleList
								});
								if(newValue.length == 1){
									$(".inchargePeople").xcombobox("setValue",newValue[0]);
								}
							}else{
								$(".inchargePeople").xcombobox({
									data:[]
								});
							}
						}
					});
					$(".concernList").xcombobox({
						data:dat.rows,
						onChange:function(newValue,oldValue){
							selectedConcerns = newValue;
							var chargePeopleNameList = [];
							if(newValue.length > 0 && dat.rows.length > 0){
								for(var i=0;i<newValue.length;i++){
									for(var j=0;j<dat.rows.length;j++){
										if(newValue[i] == dat.rows[j].id){
											chargePeopleNameList.push(dat.rows[j].name);
										}
									}
								}
								$(".concernList").next().children().next().attr("title",chargePeopleNameList.toString());
							}
						}
					});
				} else {
					/*$.xnotify("系统错误！", {type:'warning'});*/
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务类别列表
function loadMissionCategory(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务类别",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".missionCategory").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务紧急程度列表
function loadEmergencyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务紧急程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".emergencyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//加载任务难易程度列表
function loadDifficultyDegree(){
	$.post(
			baseUrl + "/testBaseSet/testBaseSetAction!loadTestBaseSetList.action",
			{
				"page": 1,
				"rows": 30,
				"dto.subName": "任务难易程度",
				"dto.flag": "1",
			},
			function(dat) {
				if (dat != null) {
					$(".difficultyDegree").xcombobox({
						data:dat.rows
					});
				} else {
					$.xalert({title:'提示',msg:'系统错误！'});
				}
			}, "json");
}
//@ sourceURL=createItem.js