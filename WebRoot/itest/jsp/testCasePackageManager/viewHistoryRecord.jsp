<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
  .datagrid-header-check>input{
    display:none
  }
</style>
<!--tab area-->
  <div class="tab_div">
  <!--  <ul class="tab_menu" >
       <li class="tab_li_lf active" id="roleInnerTab" ><a >角色内账户</a></li><li class="tab_li_rt " id="roleOuterTab"><a >角色外账户</a></li>
    </ul> -->
     <ul class="nav nav-lattice">
    <li class="active"><a style="cursor: pointer;" id="pkgHistoryTab"  data-toggle="tab">工期修改历史</a></li>
    <li><a style="cursor: pointer;"  id="testCasePkgHistoryTab" data-toggle="tab">用例分配历史</a></li>
  </ul>
  </div>
<!--/tab area-->

<!-- tabContext -->
<div class="tabwapper" >
	<!-- 测试包记录 -->
	<div id="pkgHistoryDiv" >
		<!--top tools area-->
		<div class="tools" style="padding-top: 10px;">
			<button type="button" class="btn btn-default" onclick="closeHistoryWin()"><i class="glyphicon glyphicon-off"></i>关闭</button>
		</div>
		<!--/.top tools area-->
		<table id="pkgHistory" data-options="
			fitColumns: true,
			rownumbers: true,
			singleSelect: true,
			pagination: true,
			pageNumber: 1,
			pageSize: 10,
			layout:['list','first','prev','manual','next','last','refresh','info'],
			pageList: [10,30,50,100]
		"></table>
	</div>
	<!-- /测试包记录 -->
	
	<!-- 用例记录 -->
    <div id="testCasePkgHistoryDiv" style="display:none" >
    <!--top tools area-->
		<div class="tools" style="padding-top: 10px;">
			<button type="button" class="btn btn-default" onclick="closeHistoryWin()"><i class="glyphicon glyphicon-off"></i>关闭</button>
		</div>
		<!--/.top tools area-->
		<table id="testCasePkgHistory" data-options="
			fitColumns: true,
			rownumbers: true,
			singleSelect: false,
			pagination: true,
			pageNumber: 1,
			pageSize: 10,
			layout:['list','first','prev','manual','next','last','refresh','info'],
			pageList: [10,30,50,100]
		"></table>
	  </div> 
	 <!-- /用例记录 -->
</div>
<!-- /tabContext -->

<script type="text/javascript" src="<%=request.getContextPath()%>/itest/js/testCasePackageMananger/viewHistoryRecord.js"></script>